<!DOCTYPE html>
<html lang="en">

<head>
    <title>KICC -Korea IT Cooperation Center in HCMC</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="kicc.vn,Korea IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="KICC -Korea IT Cooperation Center in Hanoi" />
    <meta property="og:description"
        content="About usKorea IT Cooperation Center (KICC) in HCM Established in May 2019, KICC serves as a National IT Industry Promotion Agency of Korea (NIPA)’s strategic hub for boosting more vigorous business opportunities between Korea and ASEAN countries in the ICT industry. NIPA is a non-profit government agency affiliated to the Ministry of Science and ICT to the Republic of Korea,&nbsp;Continue Reading" />
    <meta property="og:url" content="http://kicc.vn/" />
    <meta property="og:site_name" content="KICC -Korea IT Cooperation Center in Hanoi" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description"
        content="About usKorea IT Cooperation Center (KICC) in HCM Established in May 2019, KICC serves as a National IT Industry Promotion Agency of Korea (NIPA)’s strategic hub for boosting more vigorous business opportunities between Korea and ASEAN countries in the ICT industry. NIPA is a non-profit government agency affiliated to the Ministry of Science and ICT to the Republic of Korea,&nbsp;Continue Reading" />

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,600,700,800,900&display=swap"
        rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style_custom.css">

</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="/"> <img src="images/logo.png" alt="" class="logo"> </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu"></span> Menu
            </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                @if($locale == 'en')
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="#about" class="nav-link menu_en">{{ trans('messages.menu_1') }}</a>
                    </li>
                    <li class="nav-item"><a href="#Facilities"
                            class="nav-link menu_en">{{ trans('messages.menu_2') }}</a></li>
                    <li class="nav-item"><a href="#Resident" class="nav-link menu_en">{{ trans('messages.menu_3') }}</a>
                    </li>
                    <li class="nav-item"><a href="#condition"
                            class="nav-link menu_en">{{ trans('messages.menu_4') }}</a></li>
                    <li class="nav-item"><a target="_blank" href="#market"
                            class="nav-link menu_en">{{ trans('messages.menu_5') }}</a></li>
                    <li class="nav-item"><a href="#contact" class="nav-link menu_en">{{ trans('messages.menu_6') }}</a>
                    </li>
                </ul>
                @elseif($locale == 'ko')
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="#about" class="nav-link menu_ko">{{ trans('messages.menu_1') }}</a>
                    </li>
                    <li class="nav-item"><a href="#Facilities"
                            class="nav-link menu_ko">{{ trans('messages.menu_2') }}</a></li>
                    <li class="nav-item"><a href="#Resident" class="nav-link menu_ko">{{ trans('messages.menu_3') }}</a>
                    </li>
                    <li class="nav-item"><a href="#condition"
                            class="nav-link menu_ko">{{ trans('messages.menu_4') }}</a></li>
                    <li class="nav-item"><a target="_blank" href="#market"
                            class="nav-link menu_ko">{{ trans('messages.menu_5') }}</a></li>
                    <li class="nav-item"><a href="#contact" class="nav-link menu_ko">{{ trans('messages.menu_6') }}</a>
                    </li>
                </ul>
                @elseif($locale == 'vi')
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="#about" class="nav-link menu_en">{{ trans('messages.menu_1') }}</a>
                    </li>
                    <li class="nav-item"><a href="#Facilities"
                            class="nav-link menu_en">{{ trans('messages.menu_2') }}</a></li>
                    <li class="nav-item"><a href="#Resident" class="nav-link menu_en">{{ trans('messages.menu_3') }}</a>
                    </li>
                    <li class="nav-item"><a href="#condition"
                            class="nav-link menu_en">{{ trans('messages.menu_4') }}</a></li>
                    <li class="nav-item"><a target="_blank" href="#market"
                            class="nav-link menu_en">{{ trans('messages.menu_5') }}</a></li>
                    <li class="nav-item"><a href="#contact" class="nav-link menu_en">{{ trans('messages.menu_6') }}</a>
                    </li>
                </ul>
                @else
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a href="#about" class="nav-link menu_en">{{ trans('messages.menu_1') }}</a>
                    </li>
                    <li class="nav-item"><a href="#Facilities"
                            class="nav-link menu_en">{{ trans('messages.menu_2') }}</a></li>
                    <li class="nav-item"><a href="#Resident" class="nav-link menu_en">{{ trans('messages.menu_3') }}</a>
                    </li>
                    <li class="nav-item"><a href="#condition"
                            class="nav-link menu_en">{{ trans('messages.menu_4') }}</a></li>
                    <li class="nav-item"><a target="_blank" href="#market"
                            class="nav-link menu_en">{{ trans('messages.menu_5') }}</a></li>
                    <li class="nav-item"><a href="#contact" class="nav-link menu_en">{{ trans('messages.menu_6') }}</a>
                    </li>
                </ul>
                @endif

                @if($locale == 'en')
                <div class="header-menu-language-label" onclick="showModal()">EN</div>
                @elseif($locale == 'ko')
                <div class="header-menu-language-label" onclick="showModal()">KO</div>
                @elseif($locale == 'vi')
                <div class="header-menu-language-label" onclick="showModal()">VI</div>
                @else
                <div class="header-menu-language-label" onclick="showModal()">EN</div>
                @endif
            </div>
        </div>
    </nav>

    <!-- END nav -->

    <div>
        <!-- <div id="carouselExampleSlidesOnly " class="carousel slide  animation-element slide-left" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active slider-item">
                    <img class="d-block w-100" src="banner/bn1.jpg">
                    <div class="box-text ">
                        <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                        <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                    </div>
                </div>
                <div class="carousel-item active slider-item">
                    <img class="d-block w-100" src="banner/bn2.jpg">
                    <div class="box-text ">
                        <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                        <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                    </div>
                </div>

            </div>
        </div> -->


        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active slider-item">
                    <img class="d-block w-100" src="banner/bn1.jpg">
                    <div class="box-text ">
                        <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                        <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                    </div>
                </div>
                <div class="carousel-item  slider-item">
                    <img class="d-block w-100" src="banner/bn2.jpg">
                    <div class="box-text ">
                        <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                        <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <br>
        <section class="ftco-section ftco-no-pt ftco-no-pb  animation-element slide-left" id="about">

            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-12 wrap-about py-md-5 ftco-animate">
                        <div class="heading-section mb-5 pt-5 pl-md-5">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12 col-img">
                                        <h3 class="mb-4"> <img class="ft_icon" src="{{asset('sg/icon_kiccHCM.png')}}"
                                                alt="">
                                            {{ trans('messages.about') }}</h2>
                                    </div>

                                    <div class="col-md-12">
                                        @if($locale === 'vi')
                                        <p><strong>Trung tâm hợp tác Công nghệ Thông tin (KICC) ở Thành phố Hồ
                                                Chí Minh
                                            </strong></p>
                                        <p>
                                            KICC HCM thành lập ngày 23 tháng 5 năm 2019, là văn phòng thứ 06
                                            trên toàn
                                            cầu và thứ 02 tại Việt Nam. KICC là văn phòng quốc tế trực thuộc Bộ
                                            Khoa học
                                            - Công nghệ Thông tin Hàn Quốc (MSIT) và Cơ quan Xúc tiến Công nghệ
                                            Thông
                                            tin - Truyền thông (NIPA) với mục đích giúp đỡ việc kinh doanh quốc
                                            tế của
                                            những công ty về Công nghệ Thông tin (CNTT) của Hàn Quốc. KICC có 06
                                            văn
                                            phòng ở Thung lũng Silicon (Mỹ), Tokyo (Nhật), Bắc Kinh (Trung
                                            Quốc),
                                            Singapore, Hà Nội (Việt Nam) và Hồ Chí Minh (Việt Nam).
                                        </p>
                                        <p>
                                            NIPA là tổ chức phi lợi nhuận từ Chính phủ Hàn Quốc và là thành viên
                                            của Bộ
                                            Khoa học - Công nghệ Thông tin Hàn Quốc, chịu trách nhiệm trong việc
                                            hỗ trợ
                                            các công ty và các chuyên gia CNTT. NIPA hiện dẫn đầu trong kiến
                                            thức về
                                            cơ sở hạ tầng kinh tế xã hội và phát triển kinh tế quốc gia bằng
                                            việc xúc
                                            tiến khả năng cạnh tranh của toàn ngành công nghiệp thông qua sự
                                            tiến bộ kỹ
                                            thuật CNTT và công nghiệp.
                                            <ol style="padding-left: 20px !important">
                                                <li>Hỗ trợ công ty Hàn Quốc vào Việt Nam</li>
                                                <li>Đẩy mạnh nền CNTT khu vực Việt Nam và Hàn Quốc</li>
                                                <li>Hỗ trợ phát triển nền kinh tế thị trường tại Việt Nam</li>
                                            </ol>
                                        </p>
                                        @elseif($locale === 'en')
                                        <p><strong>Korea IT Cooperation Center (KICC) in HCMC</strong></p>
                                        <p>
                                            KICC HCMC is established in May 23, 2019, which is 6th office over
                                            the world
                                            and
                                            2nd office in Vietnam. KICC is overseas office for supporting Korean
                                            ICT
                                            companies’ international business under the Ministry of Science and
                                            ICT(MSIT)
                                            and National IT Industry Promotion Agency(NIPA). KICC has 6 offices
                                            in
                                            Silicon
                                            Valley(USA), Tokyo(Japan), Beijing(China), Singapore(Singapore),
                                            Hanoi(Vietnam),
                                            Ho Chi Minh(Vietnam).
                                        </p>
                                        <p>
                                            NIPA is a non-profit government agency affiliated to the Ministry of
                                            Science
                                            and
                                            ICT to the Republic of Korea, which is responsible for providing
                                            support to
                                            IT
                                            enterprises and professionals. NIPA leads national economic
                                            development and
                                            knowledge-based economic society by promoting competitiveness of
                                            overall
                                            industries through IT usage and advancing IT industries.
                                            <ol style="padding-left: 20px !important">
                                                <li> Support Korean companies participation in Vietnam</li>
                                                <li> Promote Vietnam and Korea ICT industry sector</li>
                                                <li> Support Development of a market economy in Vietnam</li>
                                            </ol>
                                        </p>
                                        @else
                                        <p><strong>Korea IT Cooperation Center (KICC) in HCMC</strong></p>
                                        <p>
                                            2019년 5월 23일에 설립된 호치민IT지원센터(KICC HCMC)는 전 세계 6번째 사무소이자 베트남에서는 2번째
                                            사무소입니다.
                                            <br />
                                            KICC는
                                            과학기술정보통신부(MSIT)와 정보통신산업진흥원 (NIPA) 산하기관으로 한국 ICT기업의 해외 사업을 지원하는
                                            지원센터입니다. <br>
                                            KICC는
                                            실리콘 밸리(미국), 도쿄(일본), 베이징(중국), 싱가포르(싱가포르), 하노이(베트남), 호치민(베트남)에 6개의 지사를
                                            두고
                                            있습니다.
                                        </p>
                                        <p>
                                            NIPA는 비영리 정부기관이며, IT기업과 전문가들을 지원하는 대한민국 과학기술정보통신부의 산하기관입니다. NIPA는
                                            IT기술 및 산업의
                                            진보를
                                            통해 산업 전반의 경쟁력을 키우는 사회경제기반 지식 및 국가경제발전을 선도하고 있습니다
                                            <ol style="padding-left: 20px !important">
                                                <li> 한국기업 베트남 참여 지원</li>
                                                <li> 베트남과 한국 ICT 산업 분야 홍보</li>
                                                <li> 베트남의 시장 경제 발전 지원</li>
                                            </ol>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
        </section>

        <section class="ftco-section ftco-no-pt ftco-no-pb animation-element slide-left" id="Facilities"
            style="background: #e8e8e8;">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-12 wrap-about py-md-5 ftco-animate">
                        <div class="heading-section mb-5 pt-5 pl-md-5">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12 col-img">
                                        <h3 class="mb-4"> <img class="ft_icon" src="{{asset('sg/icon_kiccHCM.png')}}"
                                                alt="">
                                            {{ trans('messages.title_kicc_hcm') }}</h2>
                                    </div>
                                </div>
                                <br>
                                <div class="row">

                                    <div class="col-md-4 img__wrap img__wrap_mb">
                                        <img class="img__img" src="{{asset('bn/1.png')}}" alt="" style="width:inherit">
                                        <div class="img__description_layer">
                                            <h3 class="img__description">{{ trans('messages.title_product_1') }}
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="col-md-4 img__wrap">
                                        <img src="{{asset('bn/2.png')}}" alt="" style="width:inherit">
                                        <div class="img__description_layer">
                                            <h3 class="img__description">{{ trans('messages.title_product_2') }}
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="col-md-4 img__wrap">
                                        <img src="{{asset('bn/3.png')}}" alt="" style="width:inherit">
                                        <div class="img__description_layer">
                                            <h3 class="img__description">{{ trans('messages.title_product_3') }}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 img__wrap">
                                        <img src="{{asset('bn/4.png')}}" alt="" style="width:inherit">
                                        <div class="img__description_layer">
                                            <h3 class="img__description">{{ trans('messages.title_product_4') }}
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="col-md-4 img__wrap">
                                        <img src="http://www.mhkot.net:8282/bn/5.png" alt="" style="width:inherit">
                                        <div class="img__description_layer">
                                            <h3 class="img__description">{{ trans('messages.title_product_5') }}
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="col-md-4 img__wrap">
                                        <img src="{{asset('bn/6.png')}}" alt="" style="width:inherit">
                                        <div class="img__description_layer">
                                            <h3 class="img__description">{{ trans('messages.title_product_6') }}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ftco-section ftco-no-pt ftco-no-pb animation-element slide-left" id="Resident">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-md-12 wrap-about py-md-5 ftco-animate">
                        <div class="heading-section mb-5 pt-5 pl-md-5">
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12 col-img">
                                        <h3 class="mb-4"> <img class="ft_icon" src="{{asset('sg/icon_kiccHCM.png')}}"
                                                alt="">
                                            {{ trans('messages.share_room') }}</h2>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3"> <a target="_blank" href="http://mojitok.com/"><img
                                                src="{{asset('logo/logo_64.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a target="_blank" href="http://www.buttle.co.kr/"><img
                                                src="{{asset('logo/logo_38.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a target="_blank" href="http://www.playauto.co.kr/"><img
                                                src="{{asset('logo/logo_62.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a target="_blank" href="http://nexustech.or.kr/"><img
                                                src="{{asset('logo/logo_28.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3"><a href="http://www.cocean.co.kr/" target="_blank">
                                            <img src="{{asset('logo/logo_26.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a target="_blank" href="http://ir.gabia.com/"><img
                                                src="{{asset('logo/logo_39.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a target="_blank"
                                            href="http://www.thinkforbl.com/?ckattempt=1"><img
                                                src="{{asset('logo/logo_40.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a target="_blank" href="http://www.zmot.co.kr/"><img
                                                src="{{asset('logo/logo_41.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3"> <img src="{{asset('logo/logo_46.jpg')}}" alt=""
                                            class="img-thumbnail"> </div>
                                    <div class="col-md-3"><a target="_blank" href="http://www.designmdesign.com/"><img
                                                src="{{asset('logo/logo_47.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><img src="{{asset('logo/logo_48.jpg')}}" alt=""
                                            class="img-thumbnail"></div>
                                    <div class="col-md-3"><a target="_blank"
                                            href="https://www.jandi.com/landing/kr"><img
                                                src="{{asset('logo/logo_49.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3"> <a target="_blank" href="http://www.f1security.co.kr/"><img
                                                src="{{asset('logo/logo_54.jpg')}}" alt="" class="img-thumbnail"> </a>
                                    </div>
                                    <div class="col-md-3"><img src="{{asset('logo/logo_55.jpg')}}" alt=""
                                            class="img-thumbnail"></div>
                                    <div class="col-md-3"><a target="_blank" href="https://www.ta-fun.co.kr/"><img
                                                src="{{asset('logo/logo_56.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a href="http://www.ylabs.kr/pc/" target="_blank"><img
                                                src="{{asset('logo/logo_57.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-md-3"> <a target="_blank" href="https://iportfolio.co.kr/"><img
                                                src="{{asset('logo/logo_32.jpg')}}" alt="" class="img-thumbnail"> </a>
                                    </div>
                                    <div class="col-md-3"><img src="{{asset('logo/logo_63.jpg')}}" alt=""
                                            class="img-thumbnail"></div>
                                    <div class="col-md-3"><a target="_blank" href="https://www.mixtainment.com/"><img
                                                src="{{asset('logo/logo_66.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a target="_blank" href="http://bicube.co.kr/"><img
                                                src="{{asset('logo/logo_72.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3"><a target="_blank" href="http://www.jp.or.kr/"><img
                                                src="{{asset('logo/logo_73.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"><a target="_blank" href="http://www.sqisoft.com/ko/main"><img
                                                src="{{asset('logo/logo_74.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                    <div class="col-md-3"> <a target="_blank" href="https://wincert.com/"><img
                                                src="{{asset('logo/logo_30.jpg')}}" alt="" class="img-thumbnail"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </section>
        <section id="condition">
            <div class="wraper">
                <img src="{{asset('sg/item_cacbuoc.png')}}" alt="" class="img_cacbuoc">
            </div>
            <div class="wraper2" id="market">
                <div class="wp">
                    @if($locale == 'ko')
                    <div class="text-center">
                        <h3 class="mb-4">베트남 시장 정보</h3>
                    </div>
                    <p>베트남 통계청에 따르면 IT 산업 내 등록된 기업 수가 가장 많은 부문은 소프트웨어로 <br />
                        2013년 기준 총 6,832개 기업이 있으며, IT 산업 수익에서 가장 큰 비중을
                        차지하는 하드웨어는
                        <br /> 2013년 연 59.7%의 성장률을 보임
                    </p>
                    <p>2013년 베트남 브로드밴드 가입자 수는 2,237만 명으로 24.93%의 보급률을 보였으며, <br />
                        그 중 3G 모바일 브로드밴드 가입자 수가 1,722만 명으로 약 77%를
                        차지함
                    </p>
                    <p>베트남 정부는 현재 초고속 국가 정보망 구축 계획을 수립, 인터넷 보급과 브로드밴드 네트워크 확대를 위한 정책을 <br /> 진행하고 있어 IPTV, VoIP 등의 서비스
                        시장이
                        본격적으로 발전할 가능성이 높음</p>
                    <div class="text-center"><a target="_blank"
                            href="https://www.globalict.kr/jsp/country/country.jsp?menuCode=010309&catNo=41&p_catNo=27&p_catNm"><img
                                class="img-thumbnail logo_dk" src="{{asset('sg/logo_dk.png')}}" alt=""></a>
                        <a style="color:#53565a !important" target="_blank"
                            href="https://www.globalict.kr/jsp/country/country.jsp?menuCode=010309&catNo=41&p_catNo=27&p_catNm"
                            style="margin-bottom:0 !important">
                            <p style="margin-bottom:0 !important">[자세히 보기]</p>
                        </a>
                    </div>
                    @elseif($locale == 'vi')
                    <div class="text-center">
                        <h3 class="mb-4">THÔNG TIN THỊ TRƯỜNG VIỆT NAM</h3>
                    </div>
                    <p>Theo Cục Thống kê Việt Nam, lĩnh vực có số lượng doanh nghiệp đăng ký nhiều nhất trong ngành CNTT
                        là lĩnh vực Phần mềm, với tổng số 6.832 doanh nghiệp tính đến năm 2013; và lĩnh vực chiếm tỷ
                        trọng lớn nhất trong tổng doanh thu của ngành CNTT là lĩnh vực Phần cứng, với tỉ lệ tăng trưởng
                        đạt 59,7% trong năm 2013.
                    </p>
                    <p>Năm 2013, thuê bao băng thông rộng của Việt Nam đạt 22,37 triệu, với tỷ lệ phổ cập là 24,93%,
                        trong đó thuê bao băng thông rộng di động 3G chiếm 17,22 triệu, tương đương khoảng 77%.
                    </p>
                    <p>Chính phủ Việt Nam hiện đang thiết lập kế hoạch xây dựng mạng thông tin quốc gia tốc độ cao và
                        đang thực hiện các chính sách để phổ cập Internet, mở rộng mạng băng thông rộng. Vì vậy rất có
                        khả năng các thị trường dịch vụ như IPTV, VoIP,... sẽ phát triển mạnh mẽ.</p>
                    <div class="text-center"><a target="_blank"
                            href="https://www.globalict.kr/jsp/country/country.jsp?menuCode=010309&catNo=41&p_catNo=27&p_catNm"><img
                                class="img-thumbnail logo_dk" src="{{asset('sg/logo_dk.png')}}" alt=""></a>
                        <a style="color:#53565a !important" target="_blank"
                            href="https://www.globalict.kr/jsp/country/country.jsp?menuCode=010309&catNo=41&p_catNo=27&p_catNm"
                            style="margin-bottom:0 !important">
                            <p style="margin-bottom:0 !important">[Xem chi tiết] </p>
                        </a>
                    </div>
                    @elseif($locale == 'en')
                    <div class="text-center">
                        <h3 class="mb-4">VIETNAM MARKET INFORMATION</h3>
                    </div>
                    <p>According to the General Statistics Office of Viet Nam, the sector with the largest number of
                        registered companies in the IT industry is the software, with a total of 6,832 companies in
                        2013; and the sector that accounts for the largest share of total IT revenues is the hardware,
                        grew at 59.7% in 2013.
                    </p>
                    <p>In 2013, Vietnam's broadband subscribers reached 22.37 million, with a universal rate of 24.93%,
                        of which 3G mobile broadband subscribers accounted for 17.22 million, equivalent to about 77%.
                    </p>
                    <p>The Government of Vietnam is currently setting up a plan to build a high-speed national
                        information network and is implementing policies to popularize the Internet and expand broadband
                        networks. Service markets such as IPTV and VoIP are likely to open in earnest.</p>
                    <div class="text-center"><a target="_blank"
                            href="https://www.globalict.kr/jsp/country/country.jsp?menuCode=010309&catNo=41&p_catNo=27&p_catNm"><img
                                class="img-thumbnail logo_dk" src="{{asset('sg/logo_dk.png')}}" alt=""></a>
                        <a style="color:#53565a !important" target="_blank"
                            href="https://www.globalict.kr/jsp/country/country.jsp?menuCode=010309&catNo=41&p_catNo=27&p_catNm"
                            style="margin-bottom:0 !important">
                            <p>[Read more]</p>
                            </p>
                    </div>
                    @endif
                </div>

            </div>
        </section>

        <footer class="ftco-footer ftco-bg-dark ftco-section animation-element slide-left" id="contact">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-md-6">
                        <div class="ftco-footer-widget mb-4">
                            <div class="heading-left text-center col-footer">

                                <h3 class="text-footer2"> <img class="ft_icon" src="{{asset('bn/icon.png')}}" alt="">
                                    {{ trans('messages.contact') }}</h3>
                            </div>
                            <br>
                            <p><strong style="color:#fff">Korea IT Cooperation Center (KICC) in HCMC</strong>
                            </p>
                            <ul class="footer-info">
                                <li><img src="{{asset('images/icon_03.jpg')}}" alt=""> <span>Address: 135 Hai Ba
                                        Trung,
                                        Dicstric 1 , HCMC </span>
                                </li>
                                <li><img src="{{asset('images/icon_06.jpg')}}" alt="">
                                    <span>Hotline: 84 28 35208135 (Vietnamese) | 84 28 35208136 (Korean) </span>
                                </li>
                                <li><img src="{{asset('images/icon_09.jpg')}}" alt=""> <span>Email:
                                        HCMC@nipa.kr</span>
                                </li>
                                <li><img src="{{asset('images/icon_12.jpg')}}" alt=""> <span>Website: <a href=""
                                            class="text-web">http://kicc.vn/</a> </span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="ftco-footer-widget mb-4">
                            <iframe width="100%" height="250" style="" id="gmap_canvas"
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.3782832778293!2d106.69636621480092!3d10.782311892317537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f36156267c7%3A0xfb2f71de82868922!2zMTM1IEhhaSBCw6AgVHLGsG5nLCBC4bq_biBOZ2jDqSwgUXXhuq1uIDEsIEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1sen!2s!4v1563956259794!5m2!1sen!2s"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">

                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;
                            <script>
                            document.write(new Date().getFullYear());
                            </script> KICC.vn Designer by <a href="http://hanbisoft.com/">Hanbisoft</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
            aria-hidden="true" id="myModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <ul class="navbar-nav ml-auto ml_language" style="margin-left:0 !important">
                        <li>
                            <a href="{!! route('user.change-language', ['en']) !!}">English</a>
                        </li>
                        <li>
                            <a href="{!! route('user.change-language', ['vi']) !!}">Tiếng Việt</a>
                        </li>
                        <li>
                            <a href="{!! route('user.change-language', ['ko']) !!}">한국어</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- loader -->
        <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
                <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
                <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                    stroke="#F96D00" /></svg>
        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-migrate-3.0.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/aos.js"></script>
        <script src="js/jquery.animateNumber.min.js"></script>
        <script src="js/scrollax.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false">
        </script>
        <script src="js/google-map.js"></script>
        <script src="js/main.js"></script>
        <img id="button" src="{{asset('images/top.png')}}" alt="" class="show">
        <script>
        var btn = $("#button");
        $(window).scroll(function() {
            $(window).scrollTop() > 300 ? btn.addClass("show") : btn.removeClass("show")
        }), btn.on("click", function(o) {
            o.preventDefault(), $("html, body").animate({
                scrollTop: 0
            }, "300")
        });
        </script>
        <script>
        var $animation_elements = $('.animation-element');
        var $window = $(window);

        function check_if_in_view() {
            var window_height = $window.height();
            var window_top_position = $window.scrollTop();
            var window_bottom_position = (window_top_position + window_height);

            $.each($animation_elements, function() {
                var $element = $(this);
                var element_height = $element.outerHeight();
                var element_top_position = $element.offset().top;
                var element_bottom_position = (element_top_position + element_height);

                //check to see if this current container is within viewport
                if ((element_bottom_position >= window_top_position) &&
                    (element_top_position <= window_bottom_position)) {
                    $element.addClass('in-view');
                } else {
                    $element.removeClass('in-view');
                }
            });
        }

        $window.on('scroll resize', check_if_in_view);
        $window.trigger('scroll', check_if_in_view);

        function showModal() {
            $('#myModal').modal('show')
        }
        </script>

</body>

</html>

</html>