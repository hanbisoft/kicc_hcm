@extends('backend.master_admin')
@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>Create New or History</h2>
        <br>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#home"> <img src="{{asset('images/eng.png')}}" alt="">English</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#menu1"><img src="{{asset('images/vie.png')}}" alt="">Việt Nam</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#menu2"><img src="{{asset('images/kor.png')}}" alt="">Korean</a>
            </li>
        </ul>
        <form action="{{route('post.store')}}" enctype="multipart/form-data" method="post" style="margin-bottom: 2em">
            @csrf
            <input type="hidden" name="day" value="">
            <input type="hidden" name="month" value="">
            <input type="hidden" name="time_end" value="">
            <input type="hidden" name="time_start" value="">
            <!-- Tab panes -->
            <div class="tab-content">
                <div id="home" class="container tab-pane active"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input maxlength="191" class="form-control" placeholder="title" name="title_en" id="title_en">
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select required name="cate_id" id="cate_id" class="form-control">
                            <option value="">Please select</option>
                            <option value="1">New</option>
                            <option value="3">History</option>
                            <option value="4">Asean</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Images</label>
                        <input accept="image/png, image/jpeg" required type="file" class="from-control" name="img"
                               id="img">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Pdf</label>
                                <input accept="application/pdf,application/vnd.ms-excel" type="file"
                                       class="from-control" name="file_pdf" id="file_pdf">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>File Excel</label>
                                <input accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                       type="file" class="from-control" name="file_excel" id="file_excel">
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_en" name="content_en" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea  maxlength="191" id="description_en" name="description_en"
                                  class="form-control"></textarea>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Create date</label>
                                <input type="date" value="{{ date("Y-m-d") }}" class="form-control" name="created_at" id="created_at">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="menu1" class="container tab-pane fade"><br>
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input maxlength="191" class="form-control" placeholder="title" name="title_vi" id="title_vi">
                    </div>
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea id="content_vi" name="content_vi" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Mô tả</label>
                        <textarea maxlength="191" id="description_vi" name="description_vi"
                                  class="form-control"></textarea>
                    </div>

                </div>
                <div id="menu2" class="container tab-pane fade"><br>
                    <div class="form-group">
                        <label>Title</label>
                        <input maxlength="191" class="form-control" placeholder="title" name="title_ko" id="title_ko">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea id="content_ko" name="content_ko" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea maxlength="191" id="description_ko" name="description_ko"
                                  class="form-control"></textarea>
                    </div>
                </div>
                <button style="margin-left: 1em" type="submit" class="btn btn-danger">Create</button>
                <br>
            </div>

        </form>
    </div>

    <script>
        $(document).ready(function () {
            $(".nav-tabs a").click(function () {
                $(this).tab('show');
            });
        });
        CKEDITOR.replace('content_vi', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        CKEDITOR.replace('content_en', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });
        CKEDITOR.replace('content_ko', {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
            filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
            filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
            filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
            filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
            filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
        });

    </script>

@endsection