@extends('backend.master_admin')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>List History</h2>
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 2em;margin-top: 2em">
                <a class="btn btn-primary float-left" href="{{asset('uploads/posts.xlsx')}}"><span><i
                                class="fas fa-file-download"></i></span> Download Template</a>
                <form style="float: right" class="form-inline" action="{{ route('admin.post.importNew') }}"
                      method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-2">
                        <input required
                               accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                               type="file" name="file" class="form-control">
                    </div>
                    <button class="btn btn-success"><i class="fas fa-upload"></i> Import</button>
                </form>
            </div>
        </div>
        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-company">
            <thead>
            <tr>
                <th>id</th>
                <th>Title (Eng)</th>
                <th>Title (Ko)</th>
                <th>Title (Vi)</th>
                <th colspan="2"></th>
            </tr>
            </thead>
        </table>
    </div>

@endsection
@push('script')

    <script>
        $(function () {
            $('#dataTables-company').DataTable({
                async: true,
                ajax: {
                    "url": "{{ route('admin.post.show-history') }}",
                    "type": "GET",
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [
                    {data: 'id'},
                    {data: 'title_en', orderable: true, searchable: true},
                    {data: 'title_ko'},
                    {data: 'title_vi'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        });

        function confirmDelete($id) {
            if (confirm("Are you sure you want to delete this item?") == true) {
                $(document).find('#frm_delete_' + $id).submit();
            } else {
                return false
            }
        }
    </script>
@endpush
