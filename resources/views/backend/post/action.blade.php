<a href='{{ route('post.edit',['id' => $new->id]) }}' class="btn btn-xs btn-primary"><i
            class="glyphicon glyphicon-edit"></i></a>
<a href="javascript:void(0);" onclick="return confirmDelete({{ $new->id }})" class="btn btn-xs btn-danger">
    <i class="glyphicon glyphicon-remove">
        <form action="{{ route('company.destroy',$new->id) }}" method="post" id="frm_delete_{{ $new->id }}">
            {{csrf_field() }}
            {{method_field('DELETE')}}
        </form>
    </i>
</a>