@extends('backend.master_admin')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>List Book</h2>

       <div class="table-responsive-md" style="overflow:scroll;">
           <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-company">
               <thead>
               <tr>
                   <th>id</th>
                   <th>Company name</th>
                   <th>Email</th>
                   <th>Phone</th>
                   <th>Room</th>
                   <th>Start date</th>
                   <th>End date</th>
                   <th></th>
               </tr>
               </thead>
           </table>
       </div>
    </div>

@endsection
@push('script')

    <script>
        $(function () {
            $('#dataTables-company').DataTable({
                async: true,

                ajax: {
                    "url": "{{ route('api.showListBook') }}",
                    "type": "GET",
                    "headers": {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                "order":[[5,'DESC']],
                columns: [
                    {data: 'id'},
                    {data: 'company_name', },
                    {data: 'email'},
                    {data: 'phone'},
                    {data: 'room_name'},
                    {data: 'start_date',orderable: true, searchable: true},
                    {data: 'end_date'},
                    {data: 'action', name: 'action'},
                ]
            });
        });

        function confirmDelete($id) {
            if (confirm("Are you sure you want to delete this item?") == true) {
                $(document).find('#frm_delete_' + $id).submit();
            } else {
                return false
            }
        }
    </script>
@endpush
