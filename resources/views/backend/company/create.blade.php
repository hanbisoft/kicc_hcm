@extends('backend.master_admin')
@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </div>
    </div>
    <div class="container mt-3">
        <h2>Create company</h2>
        <br>
        <form action="{{route('company.store')}}" enctype="multipart/form-data" method="post"
              style="margin-bottom: 2em">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Images</label>
                        <input value="uploads/noimage.jpg" type="file" class="from-control" name="img" id="img">
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="">Category</label>
                    <select onchange="displayForm()"  name="cate_id" class="form-control" id="cate_id" required>
                        <option >Please select company type</option>
                        <option value="2">Incubating</option>
                        <option value="3">B2b matching</option>
                    </select>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>File Pdf</label>
                        <input accept="application/pdf,application/vnd.ms-excel" type="file"
                               class="from-control" name="file_pdf" id="file_pdf">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>File Excel</label>
                        <input accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                               type="file" class="from-control" name="file_excel" id="file_excel">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Company name (Eng)</label>
                        <input required class="form-control" type="text" value="" name="company_name_en">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Company name (Ko)</label>
                        <input class="form-control" type="text" value="" name="company_name_ko">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Company name (Vi)</label>
                        <input class="form-control" type="text" value="" name="company_name_vi">
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="hide1">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Company tel</label>
                            <input class="form-control" type="text" value="" name="tel1" >
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Kind</label>
                            <input class="form-control" type="text" value="" name="kind">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date</label>
                        <input class="form-control" type="date" value="" name="date" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Company type</label>
                        <input class="form-control" type="text" value="" name="type">
                    </div>
                </div>

            </div>
            <div id="hide2">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Owner name</label>
                            <input class="form-control" type="text" value="" name="owmer">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Owner tel</label>
                            <input class="form-control" type="text" value="" name="otel" >
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Manager</label>
                            <input class="form-control" type="text" value="" name="manger">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Manager tel</label>
                            <input class="form-control" type="text" value="" name="mtel">
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email 1</label>
                        <input class="form-control" type="text" value="" name="email1">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email 2</label>
                        <input class="form-control" type="text" value="" name="email2">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Website</label>
                        <input class="form-control" type="text" value="" name="website">
                    </div>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>About us En</label>
                            <textarea class="form-control"  name="about_us_en"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>About us Ko</label>
                            <textarea class="form-control"  name="about_us_ko"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>About us Vi</label>
                            <textarea class="form-control"  name="about_us_vi"></textarea>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Number of korean employee</label>
                        <input class="form-control" type="text" value="" name="emp_ko">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Number of Viet Nam employee</label>
                        <input class="form-control" type="text" value="" name="emp_VN">
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Area</label>
                        <input class="form-control" type="text" value="" name="area">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Location</label>
                        <input class="form-control" type="text" value="" name="location">
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Address</label>
                        <input class="form-control" type="text" value="" name="address">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Service (Eng)</label>
                        <input class="form-control" type="text" value="" name="service_en">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Service (Vi)</label>
                        <input class="form-control" type="text" value="" name="service_vi">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Service (Ko)</label>
                        <input class="form-control" type="text" value="" name="service_ko">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-danger">Create</button>
                </div>
            </div>
        </form>
    </div>
    <script>
        function displayForm(id)
        {
            var select_id = document.getElementById("cate_id");
            console.log(select_id.options[select_id.selectedIndex].value)
            if(select_id.options[select_id.selectedIndex].value == 2){
                $("#hide1").hide();
                $("#hide2").hide();
            }else {
                $("#hide1").show();
                $("#hide2").show();
            }
        }
    </script>

@endsection
