@extends('master')
@section('style')
    <link rel="stylesheet" href="{{asset('stylelogin/fonts/material-icon/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{asset('stylelogin/css/style.css')}}">
@endsection
@section('content')

    <div class="body">
        <div class="main">.

            <section class="signup">
                <!-- <img src="images/signup-bg.jpg" alt=""> -->
                <div class="container23">
                    <div class="signup-content">
                        <form id="signup-form" class="signup-form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <h2 class="form-title">Login</h2>
                            <div class="form-group">
                                <input autofocus type="email"
                                       class="form-input {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                       id="email" placeholder="Your Email" value="{{ old('email') }}" required/>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password"
                                       class="form-input {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" id="password" placeholder="Password"/>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" id="submit" class="form-submit" value="Login"/>
                            </div>
                        </form>
                        <p class="loginhere">
                            <a href="/password/reset">Lost your password?</a> <br>
                            Do not have an account ? <a href="{{route('register')}}" class="loginhere-link">Register
                                here</a>
                        </p>
                    </div>
                </div>
            </section>

        </div>
    </div>

@endsection