<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('dist/vendor.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('dist/theme.css')}}" type="text/css" rel="stylesheet" media="all">
    <title>Welcome</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <img src="{{asset('sg/banner_right.PNG')}}" class="img-responsive" alt="">
            </div>
            <div class="col-md-6">
                <img src="{{asset('sg/banner_left.PNG')}}" class="img-responsive" alt="">
            </div>
        </div>
    </div>
</body>
</html>
