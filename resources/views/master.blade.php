<!DOCTYPE html>
<!-- saved from url=(0016)http://localhost -->
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>KICC -Korea IT Cooperation Center in HCMC</title>
    <meta name="keywords" content="kicc.vn,Korea IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="KICC -Korea IT Cooperation Center in Hanoi"/>
    <meta property="og:description"
          content="About usKorea IT Cooperation Center (KICC) in Hanoi Established in November 2017, KICC serves as a National IT Industry Promotion Agency of Korea (NIPA)’s strategic hub for boosting more vigorous business opportunities between Korea and ASEAN countries in the ICT industry. NIPA is a non-profit government agency affiliated to the Ministry of Science and ICT to the Republic of Korea,&nbsp;Continue Reading"/>
    <meta property="og:url" content="http://kicc.vn/"/>
    <meta property="og:site_name" content="KICC -Korea IT Cooperation Center in Hanoi"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:description"
          content="About usKorea IT Cooperation Center (KICC) in Hanoi Established in November 2017, KICC serves as a National IT Industry Promotion Agency of Korea (NIPA)’s strategic hub for boosting more vigorous business opportunities between Korea and ASEAN countries in the ICT industry. NIPA is a non-profit government agency affiliated to the Ministry of Science and ICT to the Republic of Korea,&nbsp;Continue Reading"/>
    <!--Fonts-->

    <link rel="icon" href="{{asset('images/kicc.png')}}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Styles-->
    <link href="{{asset('dist/vendor.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('dist/theme.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('css/style_khanh.css')}}" rel="stylesheet">
    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Tajawal:400,500,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <!--Icons fonts-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    @yield('style')
    <script src="{{asset('dist/vendor-bootstrap.js')}}" type="text/javascript" defer></script>
    <script src="{{asset('dist/custom.js')}}" type="text/javascript" defer></script>

    <script src="{{asset('dist/custom.js')}}" type="text/javascript" defer></script>
    @if (\Route::current()->getName() != 'client.book.index')
        @if (\Route::current()->getName() != 'client.book-lager')
            @if (\Route::current()->getName() != 'client.book-small')
                @if (\Route::current()->getName() != 'client.book-open')
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                @endif
            @endif
        @endif
    @endif


</head>
<body>
<header class="header-hb">
    <nav class="navbar navbar-expand-lg navbar-light nav-hb" style="height: 80px;">
        <div class="container">
            <a class="navbar-brand logo" href="/">
                <img src="{{asset('images/logo.png')}}" alt="" class="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto" style="margin-left: auto !important;margin-right: 0 !important;" id="menu">
                </ul>
            </div>
            <div class="nav-right" style="margin-left: 1em;">
                <ul class="nav-current">
                    <li><a href="{!! route('user.change-language', ['en']) !!}"><img
                                    src="{{asset('images/1Main시안_최종_06.jpg')}}" alt=""></a></li>
                    <li><a href="{!! route('user.change-language', ['vi']) !!}"><img
                                    src="{{asset('images/1Main시안_최종_08.jpg')}}" alt=""></a></li>
                    <li><a href="{!! route('user.change-language', ['ko']) !!}"><img
                                    src="{{asset('images/1Main시안_최종_10.jpg')}}" alt=""></a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<main class="main-hb">
    @yield('content')
</main>
<img id="button" src="{{asset('images/top.png')}}" alt="">
<footer class="footer-hb section-hb animation-element slide-left footer_bt">
    <div class="footer-top heading">
        <div class="container" id="contact">
            <div class="row">
                <div class="col-md-6 footer-left">
                    <div class="heading">
                        <div class="heading-left text-center">
                            <img src="{{asset('bn/icon.png')}}" alt="">
                            <h3 class="text-footer">Contact us</h3>
                        </div>
                    </div>
                    <h5 class="text-footer">KOREA IT COOPERATION CENTER IN HCMC </h5>
                    <ul class="footer-info">
                        <li><img src="{{asset('images/icon_03.jpg')}}" alt=""> <span>Address: 135 Hai Ba Trung, Dicstric 1 , HCMC </span>
                        </li>
                        <li><img src="{{asset('images/icon_06.jpg')}}" alt="">
                            <span>Hotline:  (+84)35208135 (Vietnamese) | (+84)35208136 (Korean) </span></li>
                        <li><img src="{{asset('images/icon_09.jpg')}}" alt=""> <span>Email:  HCMC@nipa.kr</span>
                        </li>
                        <li><img src="{{asset('images/icon_12.jpg')}}" alt=""> <span>Website: <a href=""
                                                                                                 class="text-web">http://kicc.vn/</a> </span>
                        </li>
                    </ul>

                </div>

                <div class="col-md-6 footer-right" style="text-align:right;padding-top:1%">
                
                <iframe width="100%" height="250" style="" id="gmap_canvas"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.3782832778293!2d106.69636621480092!3d10.782311892317537!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f36156267c7%3A0xfb2f71de82868922!2zMTM1IEhhaSBCw6AgVHLGsG5nLCBC4bq_biBOZ2jDqSwgUXXhuq1uIDEsIEjhu5MgQ2jDrSBNaW5o!5e0!3m2!1sen!2s!4v1563956259794!5m2!1sen!2s"
                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
            </div>
        </div>
    </div>
   
    <div class="footer-bottom">
        <div class="container">
            <p class="text-center">© 2019 KICC.vn Designer by <a href="http://hanbisoft.com">Hanbisoft</a></p>
        </div>
    </div>

</footer>


<script>
    var btn = $("#button");
    $(window).scroll(function () {
        $(window).scrollTop() > 300 ? btn.addClass("show") : btn.removeClass("show")
    }), btn.on("click", function (o) {
        o.preventDefault(), $("html, body").animate({scrollTop: 0}, "300")
    });

    this.showMenu();
    function showMenu()
    {
        var htmlResult = "";
        var html = "";
        var html2 = "";
        $.ajax({
            url: "{{ route('api.menu') }}",
            type: "get",
            dateType: "text",
            success: function (result) {
                Object.keys(result).forEach(function(key) {
                    if(result[key].id == 5)
                    {
                        html += " <li class='nav-item'><a target='_blank' class='nav-link' href='"+result[key].url+"'>"+result[key].name+"</a></li>";
                    }
                    if(result[key].id != 5 && result[key].id != 7)
                    {
                        htmlResult += " <li class='nav-item'><a  class='nav-link' href='"+result[key].url+"'>"+result[key].name+"</a></li>";
                    }
                    if(result[key].id == 7)
                    {
                        html2 += " <li class='nav-item'><a  class='nav-link' href='"+result[key].url+"'>"+result[key].name+"</a></li>";
                    }
                  })
                $("#menu").append(htmlResult);
                $("#menu").append(html);
                $("#menu").append(html2);
            }
        });
    }
</script>
</body>
</html>
