<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="{{asset('dist/vendor.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('dist/theme.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('css/style-mobile.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{asset('css/style_khanh.css')}}" type="text/css" rel="stylesheet" media="all">
    <title>KICC -Korea IT Cooperation Center in Hanoi</title>
    <script src="{{asset('dist/vendor-bootstrap.js')}}" type="text/javascript" defer ></script>
    <script src="{{asset('dist/custom.js')}}" type="text/javascript" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="background-color: #ffffff !important;">
        <a class="navbar-brand" href="/mobile">   <img src="{{asset('images/logo.png')}}" alt="" style="max-width: 125px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{route('about.index')}}">{{ trans('messages.about') }} <span
                                class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/mobile/new">{{ trans('messages.news') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"
                       href="/mobile/company">{{ trans('messages.company_infomation') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/mobile/showroom">{{ trans('messages.showroom') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('support')}}">{{ trans('messages.support') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('asean')}}">{{trans('messages.asean')}}</a>
                </li>
            </ul>
            @guest
                <span> <a href="{{route('login')}}"
                          class="btn btn_button">{{ trans('messages.login') }}</a></span>
            @endguest

            <ul class="navbar-nav mr-auto">
                @auth
                    <li>
                        <ul class="navbar-nav mr-auto" style="margin-right: 0">
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fa fa-user-circle" aria-hidden="true"></i> <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{route('user.profile')}}">User Profile</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                        </ul>
                    </li>
                @endauth
            </ul>
             <a href="{!! route('user.change-language', ['en']) !!}"><img
                                src="{{asset('images/1Main시안_최종_06.jpg')}}" alt=""></a>
                <a href="{!! route('user.change-language', ['vi']) !!}"><img src="{{asset('images/1Main시안_최종_08.jpg')}}" alt=""></a>
               <a href="{!! route('user.change-language', ['ko']) !!}"><img
                                src="{{asset('images/1Main시안_최종_10.jpg')}}" alt=""></a>

        </div>
    </nav>
</header>
@yield('content')
<footer class="footer-hb section-hb animation-element slide-left footer_bt">
    <div class="footer-top heading">
        <div class="container">
            <div class="row">
                <div class="col-md-8 footer-left">
                    <div class="heading">
                        <div class="heading-left text-center">
                            <img src="{{asset('images/icon-footer.jpg')}}" alt=""><h3 class="text-footer">Contact us</h3>
                        </div>
                    </div>
                    <h5 class="text-footer">KOREA IT COOPERATION CENTER IN HANOI </h5>
                    <ul class="footer-info">
                        <li><img src="{{asset('images/icon_03.jpg')}}" alt=""> <span>Address: 25th Floor, Keangnam Land Tower, Nam Tu Liem, Hanoi </span></li>
                        <li><img src="{{asset('images/icon_06.jpg')}}" alt=""> <span>Hotline: (84)24 7300 0672 (Korean </span></li>
                        <li><img src="{{asset('images/icon_09.jpg')}}" alt=""> <span>Email: kicchanoi@nipa.kr</span></li>
                        <li><img src="{{asset('images/icon_12.jpg')}}" alt=""> <span>Website: <a href="">http://kicc.vn/</a> </span></li>
                    </ul>
                </div>
                <div class="col-md-4 footer-right">
                    <div class="connect">
                    </div>

                    <div class="link-social">
                        <div class="footer-email">
                        </div>
                        <div class="footer-face">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FKICC.vn%2F&tabs=timeline&width=275&height=110&small_header=false&adapt_container_width=false&hide_cover=false&show_facepile=false&appId" width="275" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media">
                            </iframe>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="footer-middle">
        <iframe width="100%" height="400" id="gmap_canvas" src="https://maps.google.com/maps?q=kangnam%20tower%20h%C3%A0%20n%E1%BB%99i%20vi%E1%BB%87t%20nam&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p class="text-center">© 2019 KICC.vn Designer by <a href="http://hanbisoft.com">Hanbisoft</a></p>
        </div>
    </div>

</footer>
<img id="button" src="{{asset('images/top.png')}}" alt="">
@if (\Route::current()->getName() != 'mobile.book')
    <script src="{{asset('dist/vendor-jquery.js')}}" type="text/javascript"></script>
@endif
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script>
    var btn=$("#button");$(window).scroll(function(){$(window).scrollTop()>300?btn.addClass("show"):btn.removeClass("show")}),btn.on("click",function(o){o.preventDefault(),$("html, body").animate({scrollTop:0},"300")});
</script>
</body>
</html>