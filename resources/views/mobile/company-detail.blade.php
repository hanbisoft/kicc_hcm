@extends('mobile.mobile-master')
@section('content')
<main class="main-hb">
    <div id="slider-nb" class="slider-nb section-hb"></div>
    <div class="search-hb section-hb animation-element slide-left"></div>
    <div class="container">
        <div class="content">
            <section class="section-hb incubat-hb animation-element slide-left">
                <div class="table-responsive">
                    <table class="table table-bordered table2">
                        <tr>
                            <td colspan="4" class="bg">
                                <h2 style="font-size: 24px" class="text-center"> {{$company->company_name_ko}}</h2>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="bg text-center">Company name (Eng)</th>
                            <td colspan="3" class="bg2">{{$company->company_name_en}}</td>
                        </tr>
                        <tr>
                            <th scope="col" class="bg text-center">Web Site</th>
                            <td class="bg2"> {{$company->website}}</td>
                            <th scope="col" class="bg text-center">Owner</th>
                            <td class="bg2">{{$company->owner}}</td>
                        </tr>
                        <tr>
                            <th scope="col" class="bg text-center">Kind</th>
                            <td class="bg2">{{$company->kind}}</td>
                            <th scope="col" class="text-center bg">Manager</th>
                            <td class="bg2">{{$company->manager}}</td>

                        <tr>
                            <th scope="col" class="text-center bg">Tel 1</th>
                            <td class="bg2">{{$company->tel1}}</td>
                            <th scope="col" class="text-center bg">Mtel 1</th>
                            <td class="bg2">{{$company->mtel1}}</td>
                        </tr>
                        <tr>
                            <th scope="col" class="text-center bg">Email 1</th>
                            <td class="bg2">{{$company->email1}}</td>
                            <th scope="col" class="text-center bg">Email 2</th>
                            <td class="bg2">{{$company->email2}}</td>
                        </tr>
                        <tr>
                            <th scope="col" class="text-center bg">Class 1</th>
                            <td class="bg2">{{$company->class1}}</td>
                            <th scope="col" class="text-center bg">Class 2</th>
                            <td class="bg2">{{$company->class2}}</td>
                        </tr>
                        <tr>
                            <th scope="col" class="text-center bg">Sale 1 (2017)</th>
                            <td class="bg2">{{$company->sale1}}</td>
                            <th scope="col" class="text-center bg">Sale 2 (2018)</th>
                            <td class="bg2">{{$company->sale2}}</td>
                        </tr>
                        <tr>
                            <th scope="row" class="text-center bg">Address</th>
                            <td class="bg2" colspan="3">{{$company->address}}</td>
                        </tr>
                        <tr>
                            <th colspan="4" class="text-center bg">
                                About us
                            </th>
                        </tr>
                        <tr>
                            <td colspan="4" class="bg2 text-justify">
                                {{$company->about_us}}
                            </td>
                        </tr>

                    </table>
                    <div style="margin: 0 auto;text-align: center;margin-bottom: 1em">
                        <a href="{{route('client.company.index')}}" class="btn btn-primary"
                           style="height: auto;background: #204181;color: #fff;width: 100px; -webkit-border-radius: 3rem; -moz-border-radius: 3rem;border-radius: 3rem;">Back</a>

                    </div>
                </div>
            </section>
        </div>
</main>
@endsection

