@extends('master')
@section('content')
    <main class="main-hb">
        {{--<div id="slider-nb" class="slider-nb section-hb"></div>--}}
        <div class="section-top"></div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="content">
                    @if($locale === 'en')
                        <h5><a>{{$event->title_en}}</a></h5>
                        <p class="description">{!! $event->content_en !!}</p>
                    @elseif($locale === 'vi')
                        <h5><a>{{$event->title_vi}}</a></h5>
                        <p class="description">{!! $event->content_vi !!}</p>
                    @else
                        <h5><a>{{$event->title_ko}}</a></h5>
                        <p class="description">{!! $event->title_ko !!}</p>
                    @endif
                    <p class="description">{{ $new->created_at->format('Y/m/d') }}</p>
                </div>
            </div>
        </section>
    </main>

@endsection
