@extends('master')
@section('content')
    <main class="main-hb">
        {{--<div id="slider-nb" class="slider-nb section-hb"></div>--}}
        <div class="section-top"></div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="content">
                    <div class="row">
                        <div class="col-md-10">

                            @if($locale == 'en')
                                 <h3 class="title">{{$new->title_en}}</h3>
                            @elseif($locale === 'vi' && $new->title_vi != null)
                                <h3 class="title">{{$new->title_vi}}</h3>
                            @elseif($locale === 'ko' && $new->title_ko != null)
                                <h3 class="title">{{$new->title_ko}}</h3>
                            @else
                                <h3 class="title">{{$new->title_en}}</h3>
                            @endif

                            <p class="description">{{ $new->created_at->format('Y/m/d') }}</p>
                        </div>
                        <div class="col-md-2">
                            @if($new->file_excel != null || $new->file_pdf != null)
                                <a onclick="showPopup()" class='btn btn-hb btn_download2'>Download</a>
                            @endif
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="social-new">
                                <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FKICC.vn%2F%3Fepa%3DSEARCH_BOX&width=134&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId"
                                        width="134" height="46" style="border:none;overflow:hidden" scrolling="no"
                                        frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 detail-text">

                            @if($locale == 'en')
                                {!! $new->content_en !!}
                            @elseif($locale === 'vi' && $new->title_vi != null)
                                {!! $new->content_vi !!}
                            @elseif($locale === 'ko' && $new->title_ko != null)
                                {!! $new->content_ko !!}
                            @else
                                {!! $new->content_en !!}
                            @endif
                        </div>
                    </div>
                    <br><br><br>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href='{{route('new.index')}}' class='btn btn-hb btn_download2'>Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    {{--//modal--}}
    <!-- Modal -->
    <div class="popup-product">
        <div id="myModal"  class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <h5 class="modal-title">Documents</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                        @if($new->file_excel != null)
                                            <td>{{str_replace("documents/","",$new->file_excel)}}</td>
                                            <form action="{{ route('download') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="url" value="{{ $new->file_excel }}">
                                                <td><button type="submit">Download</a></td>
                                            </form>

                                        @endif
                                    </tr>
                                    <tr>
                                        @if($new->file_pdf != null)
                                            <td>{{$new->file_pdf}}</td>
                                            <form action="{{ route('download') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="url" value="{{ $new->file_pdf }}">
                                                    <td><button type="submit">Download</a></td>
                                                </form>
                                        @endif
                                    </tr>
                            </table>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <script>
        function showPopup() {
            $('#myModal').modal('show')
        }
    </script>
@endsection
