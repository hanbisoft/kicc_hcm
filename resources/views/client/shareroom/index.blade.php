@extends('master')
@section('content')
    <main class="main-hb">

        <div class="section-top"></div>
    <section class="section-hb share-hb animation-element slide-left" id="shareroom">
        <div class="container">
            <div class="heading">
                <div class="heading-left">
                    <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                    <h3>{{ trans('messages.share_room') }}</h3>
                </div>
            </div>
            <div class="content">
                <div class="share-inner owl-carousel">
                    <div class="share-box animation-element scroll-zoom-out in-view">
                        <div class="share-img"><img src="images/lage.jpg" alt=""></div>
                        <div class="share-text">
                            <h5 class="text-book"><a href="">Large meeting room</a></h5>
                            @if (Auth::check())
                                <a href="/book/1" class="btn btn_book">Book</a>
                            @else
                                <a href="{{route('login')}}" class="btn btn_book">Login</a>
                            @endif
                        </div>
                    </div>
                    <div class="share-box animation-element scroll-zoom-out in-view">
                        <div class="share-img">
                            <img src="images/smal-office.jpg" alt="">
                        </div>
                        <div class="share-text">
                            <h5 class="text-book"><a href="">Small Meeting room </a></h5>
                            @if (Auth::check())
                                <a href="/book/2" class="btn btn_book">Book</a>
                            @else
                                <a href="{{route('login')}}" class="btn btn_book">Login</a>
                            @endif
                        </div>
                    </div>
                    <div class="share-box animation-element scroll-zoom-out in-view">
                        <div class="share-img">
                            <img src="images/co.jpg" alt="">
                        </div>
                        <div class="share-text">
                            <h5 class="text-book"><a href="">Open Co-Working Space</a></h5>
                            @if (Auth::check())
                                <a href="/book/3" class="btn btn_book">Book</a>
                            @else
                                <a href="{{route('login')}}" class="btn btn_book">Login</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="brand-hb owl-carousel">
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="images/sharelogo1.jpg" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="images/sharelogo2.jpg" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="images/sharelogo3.jpg" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="images/sharelogo4.jpg" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="images/sharelogo2.jpg" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="images/sharelogo3.jpg" alt="">
                    </div>
                    <div class="brand-item animation-element scroll-zoom-out in-view">
                        <img src="images/sharelogo4.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    </main>
@endsection