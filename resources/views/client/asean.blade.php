@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb">
            <img src="{{asset('banner/Untitled-4.jpg')}}" class="img-sl" alt="">
        </div>
        <section class="section-hb incubat-hb animation-element slide-left in-view" style="margin-top: auto">
            <div class="container">
                <div class="content">
                    <section class="section-hb incubat-hb animation-element slide-left">
                        <div class="container">
                            <div class="heading">
                                <div class="heading-left">
                                    <img src="{{asset('images/icon-chuan1.jpg')}}" alt=""><h3>{{ trans('messages.asean') }}</h3>
                                </div>
                            </div>
                            <div class="content">
                                <input type="hidden" id="locale" name="locale" value="{{$locale}}">
                                @foreach($new as $value)
                                        <div class="row media" onclick="show({{$value->id}});">
                                            <div class="col-4 new-img">
                                                <img  src="{{asset($value->images)}}"
                                                      class="rounded float-left img_new"/>
                                            </div>
                                            <div class="col-8">
                                                @if($locale == 'en')
                                                    <h5><a onclick="show({{$value->id}});">{{$value->title_en}}</a></h5>
                                                    <p class="description">{{$value->description_en}}</p>
                                                @elseif($locale == 'vi')
                                                    <h5><a onclick="show({{$value->id}});">{{$value->title_vi}}</a></h5>
                                                    <p class="description">{{$value->description_vi}}glkll','
                                                @elseif($locale == 'ko')
                                                    <h5><a onclick="show({{$value->id}});">{{$value->title_ko}}</a></h5>
                                                    <p class="description">{{$value->description_ko}}</p>
                                                @endif

                                            </div>
                                        </div>
                                @endforeach
                                <div class="float-right"> {{$new->links()}}</div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </section>
    </main>
    <!-- Modal -->
    <h5 id="wait"></h5>
    <div class="popup-product">
        <div id="new"  class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="social-new">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FKICC.vn%2F%3Fepa%3DSEARCH_BOX&width=134&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId" width="134" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        <a href="" class="btn btn-hb btn-download">download</a>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img"><div id="img"></div></div>
                            <div class="new-box">
                                <div class="new-title">
                                    <h6 id="title"> </h6>
                                    <div id="content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <script>
        function show($id) {
            var locale = document.getElementById('locale').value;
            var title = '';
            var images = '';
            var content = '';
            var socical = '';
            $.ajax({
                url: "{{ route('api.new') }}",
                beforeSend: function() { $('#wait').show(); },
                complete: function() { $('#wait').hide(); },
                type: "get",
                data: {
                    'locale': locale,
                    'id': $id
                },
                dateType: "text",
                success: function (result) {
                    console.log(result)
                    title += result.title;
                    images += "<img   class='img-fluid' src='" + result.images + "'>";
                    content += result.content;
                    $("#exampleModalLongTitle").html(title);
                    $("#title").html(title);
                    $("#img").html(images);
                    $("#content").html("<p class='description_modal'>"+content+"</p>");
                    $('#new').modal('show')
                }
            });
        }
    </script>
@endsection