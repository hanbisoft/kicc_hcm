@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb">
            <img src="{{asset('banner/Untitled-1.jpg')}}" alt="" class="img-sl">
        </div>
        <div class="container">
            <div class="content">
                <section class="section-hb incubat-hb animation-element slide-left">
                    <div class="container">
                        <div class="heading">
                            <div class="heading-left">
                                <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                                <h3>{{ trans('messages.about') }}</h3>
                            </div>
                        </div>
                        <div class="content">
                            @if($locale === 'en')
                                <div style="margin-left: auto;padding-left: 1em">
                                    <h5 class="title-korea-about">Korea IT Cooperation Center (KICC) in HCMC</h5>
                                    <br>
                                        <p class="description">Address: 135 Hai Ba Trung, Dicstric 1 , HCMC</p>
                                        <p class="description">Hotline:  (+84)35208135 (Vietnamese) | (+84)35208136 (Korean) </p>
                                        <p class="description">Website: <a href="http://kicc.vn/">http://kicc.vn/</a> </p>
                                        <p class="description">Email:  HCMC@nipa.kr) </p>
                                </div>

                            @elseif($locale === 'vi')
                                <div style="margin-left: auto;padding-left: 1em">
                                    <h5 class="title-korea-about">Trung tâm hợp tác công nghệ thông tin Hàn Quốc (KICC) tại HCMC.</h5>
                                    <br>
                                    <p class="description">Address: 135 Hai Ba Trung, Dicstric 1 , HCMC</p>
                                    <p class="description">Hotline:  (+84)35208135 (Vietnamese) | (+84)35208136 (Korean) </p>
                                    <p class="description">Website: <a href="http://kicc.vn/">http://kicc.vn/</a> </p>
                                    <p class="description">Email:  HCMC@nipa.kr) </p>
                                </div>

                            @else
                                <div style="margin-left: auto;padding-left: 1em">
                                    <h5 class="title-korea-about">Korea IT Cooperation Center (KICC) in HCMC</h5>
                                    <br>
                                    <p class="description">Address: 135 Hai Ba Trung, Dicstric 1 , HCMC</p>
                                    <p class="description">Hotline:  (+84)35208135 (Vietnamese) | (+84)35208136 (Korean) </p>
                                    <p class="description">Website: <a href="http://kicc.vn/">http://kicc.vn/</a> </p>
                                    <p class="description">Email:  HCMC@nipa.kr) </p>
                                </div>

                            @endif
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </main>
@endsection
