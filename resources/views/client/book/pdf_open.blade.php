<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Reservation</title>

    <style type="text/css">
        p, body, td, input, select, button { font-family: Tahoma, Arial, Helvetica, sans-serif; font-size: 14px; }
        body { padding: 0px; margin: 0px; background-color: #ffffff; }
        a { color: #1155a3; }
        .space { margin: 10px 0px 10px 0px; }
        .header { background: #003267; background: linear-gradient(to right, #011329 0%,#00639e 44%,#011329 100%); padding:20px 10px; color: white; box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.75); }
        .header a { color: white; }
        .header h1 a { text-decoration: none; }
        .header h1 { padding: 0px; margin: 0px; }
        .main { padding: 10px }
        select, option { padding: 4px; }
        button {
            background-color: #3c78d8;
            border: 1px solid #1155cc;
            color: #fff;
            padding: 6px 20px;
            border-radius: 2px;
            cursor: pointer;
        }
        h2
        {
            color: #1a4081;
        }
        .calendar_default_event_inner
        {
            text-align: center;
            margin: 0 auto;
            padding:0;
            padding-top: 1em;
            font-weight: bold !important;
            font-size: 14px !important
        }
        /*tbody tr {height: 100px !important;}*/
    </style>


    <!-- DayPilot library -->
    <script src="js/daypilot/daypilot-all.min.js"></script>
    <script src='{{asset('calender/lib/jquery.min.js')}}'></script>
    <!-- jsPDF: PDF generator -->
    <script src="js/jspdf/jspdf.min.js"></script>

</head>
<body>
<div class="main">
    <h2>Open Room</h2>
    <div id="dp"></div>
    <div class="space">
        <select id="pdf-orientation" hidden>
            <option value="portrait">Portrait</option>
            <option value="landscape">Landscape</option>
        </select>
        <select id="pdf-format" hidden>
            <option value="letter">Letter</option>
            <option value="a4">A4</option>
        </select>
        <button id="export" hidden>Export</button>
    </div>
</div>

<script src="{{asset('book/pdf_open.js')}}">



</script>

</body>
</html>
