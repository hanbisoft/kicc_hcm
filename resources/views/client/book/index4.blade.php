@extends('master')
@section('style')
    {{--<link rel="stylesheet" href="{{asset('css/admin.css')}}">--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='{{asset('calender/fullcalendar.min.css')}}' rel='stylesheet'/>
    <link href='{{asset('calender/fullcalendar.print.min.css')}}' rel='stylesheet' media='print'/>
    <script src='{{asset('calender/lib/moment.min.js')}}'></script>
    <script src='{{asset('calender/lib/jquery.min.js')}}'></script>
    <script src='{{asset('calender/fullcalendar.min.js')}}'></script>

@endsection
@section('content')
    <main>
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                        <a class="btn  btn-warning" href="/export-pdf-open">Print Open</a>
                    </div>
                </div>
                <div id='calendar'>
                    <a href="/book/1" class="btn btn-sm btn-default">All</a>
                    <a href="{{route('client.book-lager')}}" class="btn btn-sm btn-primary">Large meeting room
                    </a>
                    <a href="{{route('client.book-small')}}" class="btn btn-sm btn-success">Small Meeting room
                    </a>
                    <a  href="{{route('client.book-open')}}" type="button" class="btn btn-sm btn-danger">Open Co-Working
                        Space
                    </a>
                </div>

            </div>
        </section>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="{{route('client.book.new')}}" method="GET" id="book">
                        <input type="hidden" name="user_id" id="user_id" value=" {{Auth::user()->id}}">
                        <div class="modal-header">
                            <h5 class="modal-title" style="color: #1b3f82;text-align: center;font-weight: bold" id="">
                                MEETING ROOM RESERVATION</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <h5 id="wait"></h5>
                            <h5 class="text-danger" id="mess_error"></h5>
                            <div class="form-group">
                                <label for=""><strong class="text-danger">*</strong> Company name</label>
                                <input required type="text" class="form-control" value="{{Auth::user()->name}}"
                                       name="company_name" id="company_name"/>
                            </div>
                            <div class="form-group">
                                <label for="">Room</label>
                                {{$id}}
                                <select name="room_id" id="room_id" class="form-control" id="room_id">
                                    @switch($id)
                                        @case(1)
                                        <option selected value="1">Large meeting room</option>
                                        <option value="2">Small Meeting room</option>
                                        <option value="3">Open Co-Working Space</option>
                                        @break
                                        @case(2)
                                        <option selected value="2">Small Meeting room</option>
                                        <option value="1">Large meeting room</option>
                                        <option value="3">Open Co-Working Space</option>
                                        @break
                                        @case(3)
                                        <option selected value="3">Open Co-Working Space</option>
                                        <option value="2">Small Meeting room</option>
                                        <option value="1">Large meeting room</option>
                                        @break
                                    @endswitch
                                </select>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for=""> <strong class="text-danger">*</strong> Start Date </label>
                                    <input required type="date" class="form-control" name="start_date" readonly id="start_date"/>
                                </div>
                                <div class="form-group col-6">
                                    <label for=""> <strong class="text-danger">*</strong> End Date </label>
                                    <input required type="date" class="form-control" name="end_date" readonly id="end_date"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-6">
                                    <label for=""> <strong class="text-danger">*</strong> Time Start</label>
                                    <select name="time_start" class="form-control" id="time_start">
                                        <option value="9:00:00">9h am</option>
                                        <option value="10:00:00">10h am</option>
                                        <option value="11:00:00">11h am</option>
                                        <option value="12:00:00">12h am</option>
                                        <option value="13:00:00">13h pm</option>
                                        <option value="14:00:00">14h pm</option>
                                        <option value="15:00:00">15h pm</option>
                                        <option value="16:00:00">16h pm</option>
                                        <option value="17:00:00">17h pm</option>
                                    </select>
                                </div>
                                <div class="form-group col-6">
                                    <label for=""> <strong class="text-danger">*</strong> Time End</label>
                                    <select name="time_end" class="form-control" id="time_end">
                                        <option value="10:00:00">10h am</option>
                                        <option value="11:00:00">11h am</option>
                                        <option value="12:00:00">12h am</option>
                                        <option value="13:00:00">13h pm</option>
                                        <option value="14:00:00">14h pm</option>
                                        <option value="15:00:00">15h pm</option>
                                        <option value="16:00:00">16h pm</option>
                                        <option value="17:00:00">17h pm</option>
                                        <option value="18:00:00">18h pm</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="">Total number of people</label>
                                    <input type="number" class="form-control" id="qty" value="2" name="qty"/>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Note</label>
                                    <textarea name="note" style="max-height: 54px" class="form-control" id="note"
                                              cols="5"
                                              rows="5"></textarea>
                                </div>
                                <div class="text-center center">
                                    <button onclick="book()" type="button" class="book btn btn-primary">Book</button>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- Modal -->
        <script src="http://momentjs.com/downloads/moment.js"></script>
        <script src="{{ asset('book/open.js') }}"></script>
    </main>
@endsection
