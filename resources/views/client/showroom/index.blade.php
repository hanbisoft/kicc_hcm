@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb">
            <img src="{{asset('banner/Untitled-3.jpg')}}" alt="" class="img-sl">
        </div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form action="{{route('search.showroom')}}" method="GET" class="form-inline my-2 my-lg-0">
                        <input name="search" class="form-control mr-sm-2" type="search" placeholder="{{ trans('messages.search_content') }}" aria-label="Search">
                        <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <section class="section-hb showroom-hb animation-element slide-left">
            <div class="container">
                <div class="content">
                <div class="row">
                    
                        @foreach($showroom as $value)
                            <div class="col-4 showroom-box thumbnail animation-element scroll-zoom-out in-view" style="margin-bottom: 1em">
                                <a href="/showroom/{{$value->id}}"><div class="new-img"><img src="{{asset($value->images)}}" alt=""></div></a>
                                <div class="showroom-text caption">

                                    @if($locale == 'en')
                                        <h5><a href="/showroom/{{$value->id}}">{{$value->title_en}}</a></h5>
                                        <p>{!!  str_limit($value->description_en,30) !!}</p>
                                    @elseif($locale === 'vi' && $value->title_vi != null)
                                        <h5><a href="/showroom/{{$value->id}}">{{$value->title_vi}}</a></h5>
                                        <p>{!!  str_limit($value->description_vi,30) !!}</p>
                                    @elseif($locale === 'ko' && $value->title_ko != null)
                                        <h5><a href="/showroom/{{$value->id}}">{{$value->title_ko}}</a></h5>
                                        <p>{!!  str_limit($value->description_ko,30) !!}</p>
                                    @else
                                        <h5><a href="/showroom/{{$value->id}}">{{$value->title_en}}</a></h5>
                                        <p>{!!  str_limit($value->description_en,30) !!}</p>
                                    @endif
                                  
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="float-right">{{$showroom->links()}}</div>
                </div>
            </div>
        </section>
    </main>

@endsection