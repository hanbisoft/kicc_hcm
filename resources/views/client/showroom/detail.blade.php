@extends('master')

@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-hb section-hb animation-element slide-left">
                    <div class="container">
                        <div class="search-inner">
                            <form action="{{route('search.showroom')}}" method="GET" class="form-inline my-2 my-lg-0">
                                <input name="search" class="form-control mr-sm-2" type="search"
                                       placeholder="{{ trans('messages.search_content') }}" aria-label="Search">
                                <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="content">
                    <div class="row">
                        <div class="col-4 center">
                            <div id="content">
                                <div id="view">
                                    <img src="{{asset($showroom->images)}}" alt=""/>
                                </div>
                            </div>
                        </div>
                        <div class="col-8">
                            @if($locale == 'en')
                                <div class="row">
                                    <div class="col-md-8">
                                        <h5 class="title_show"><a class="card-title">{{$showroom->title_en}}</a></h5>
                                    </div>
                                    @if($showroom->file_excel != null || $showroom->file_pdf != null)
                                        <div class="col-md-4">
                                            <a onclick="showPopup()" class='btn btn-hb btn_download2'>Download</a>
                                        </div>
                                    @endif
                                </div>
                                    <p class="description2">{!! $showroom->description_en !!}</p>
                                    <br>
                                    <div>
                                        <strong class="description">TEL :</strong> {{$showroom->tel}} <br>
                                        <strong class="description">E-mail:</strong> {{$showroom->email}} <br>
                                        <strong class="description">ADDRESS:</strong> {{$showroom->address}}
                                    </div>
                                </div>
                            @elseif($locale === 'vi' && $showroom->title_vi != null)
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h5 class="title_show"><a class="card-title">{{$showroom->title_vi}}</a></h5>
                                        </div>
                                        @if($showroom->file_excel != null || $showroom->file_pdf != null)
                                            <div class="col-md-4">
                                                <a onclick="showPopup()" class='btn btn-hb btn_download2'>Download</a>
                                            </div>
                                        @endif
                                    </div>
                                        <p class="description2">{!! $showroom->description_vi !!}</p>
                                        <br>
                                        <div>
                                            <strong class="description">TEL :</strong> {{$showroom->tel}} <br>
                                            <strong class="description">E-mail:</strong> {{$showroom->email}} <br>
                                            <strong class="description">ADDRESS:</strong> {{$showroom->address}}
                                        </div>
                                    </div>

                            @elseif($locale === 'ko' && $showroom->title_ko != null)
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h5 class="title_show"><a class="card-title">{{$showroom->title_ko}}</a></h5>
                                        </div>
                                        br
                                        @if($showroom->file_excel != null || $showroom->file_pdf != null)
                                            <div class="col-md-4">
                                                <a onclick="showPopup()" class='btn btn-hb btn_download2'>Download</a>
                                            </div>
                                        @endif
                                    </div>
                                        <p class="description2">{!! $showroom->description_ko !!}</p>
                                        <br>
                                        <div>
                                            <strong class="description">TEL :</strong> {{$showroom->tel}} <br>
                                            <strong class="description">E-mail:</strong> {{$showroom->email}} <br>
                                            <strong class="description">ADDRESS:</strong> {{$showroom->address}}
                                        </div>
                                    </div>
                            @else
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h5 class="title_show"><a class="card-title">{{$showroom->title_en}}</a></h5>
                                        </div>
                                        @if($showroom->file_excel != null || $showroom->file_pdf != null)
                                            <div class="col-md-4">
                                                <a onclick="showPopup()" class='btn btn-hb btn_download2'>Download</a>
                                            </div>
                                        @endif
                                    </div>
                                        <p class="description2">{!! $showroom->description_en !!}</p>
                                        <br>
                                        <div>
                                            <strong class="description">TEL :</strong> {{$showroom->tel}} <br>
                                            <strong class="description">E-mail:</strong> {{$showroom->email}} <br>
                                            <strong class="description">ADDRESS:</strong> {{$showroom->address}}
                                        </div>
                                    </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <h5 class="title">Item Details</h5>
                            @if($locale === 'en' && $showroom->content_en != null)
                                <p class="description2">{!! $showroom->content_en !!}</p>
                            @elseif($locale === 'vi' && $showroom->content_vi != null)
                                <p class="description2">{!! $showroom->content_vi !!}</p>
                            @elseif($locale === 'ko' && $showroom->content_ko != null)
                                <p class="description2">{!! $showroom->content_ko !!}</p>
                            @else
                                <p class="description2">{!! $showroom->content_en !!}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin: 0 auto;text-align: center">
                <a href="{{route('client.showroom.index')}}" class="btn btn-primary"
                   style="height: auto;background: #204181;color: #fff;width: 100px; -webkit-border-radius: 3rem; -moz-border-radius: 3rem;border-radius: 3rem;margin-top:3em">Back</a>
            </div>
        </section>
    </main>
    {{--//modal--}}
    <!-- Modal -->
    <div class="popup-product">
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <h5 class="modal-title">Documents</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    @if($showroom->file_excel != null)
                                        <td>{{str_replace("documents/","",$showroom->file_excel)}}</td>
                                        <td><a download href="{{$showroom->file_excel}}">Download</a></td>
                                    @endif
                                </tr>
                                <tr>
                                    @if($showroom->file_pdf != null)
                                        <td>{{$showroom->file_pdf}}</td>
                                        <td><a download href="{{$showroom->file_pdf}}">Download</a></td>
                                    @endif
                                </tr>
                            </table>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <script>
        function showPopup() {
            $('#myModal').modal('show')
        }
    </script>
    <style>
        p{
            font-size: 15px !important;
            line-height: 25px !important;
            padding-top: 1em;
            font-family: 'Open Sans', sans-serif;
            color: #000 !important;
        }
    </style>
@endsection