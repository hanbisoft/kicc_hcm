@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb">
            <img src="{{asset('banner/Untitled-1.jpg')}}" alt="" class="img-sl">
        </div>
       <div class="container">
           <div class="content">
               <section class="section-hb incubat-hb animation-element slide-left">
                   <div class="container">
                       <div class="heading">
                           <div class="heading-left">
                               <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                               <h3>{{ trans('messages.about') }}</h3>
                           </div>
                       </div>
                       <div class="content">
                           @if($locale === 'en')
                               <div style="margin-left: auto;padding-left: 1em">
                                   <h5 class="title-korea-about">Korea IT Cooperation Center (KICC) in HCMC</h5>
                                   <p class="description2">Established in May 2019, KICC serves as a National IT Industry Promotion Agency of Korea (NIPA)’s strategic hub for boosting more vigorous business opportunities between Korea and ASEAN countries in the ICT industry. NIPA is a non-profit government agency affiliated to the Ministry of Science and ICT to the Republic of Korea, which is responsible for providing support to IT enterprises and professionals. NIPA leads national economic development and knowledge-based economic society by promoting competitiveness of overall industries through IT usage and advancing IT industries.</p>
                               </div>
                               <table class="table table-about" style="margin-top: 3em">
                                   <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">MISSION</h5>
                                           <p class="description2">DEVELOP THE NATIONAL AND ECONOMIC DYNAMIC THROUGH ICT,  CONNECT AND SUPPORT THE IT ENTERPRISES AND PROFESSIONALS. ELIMINATE BARRIERS, CONVERGENCE OF ICT INDUSTRY</p>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">PURPOSE</h5>
                                           <p class="description2">
                                               WE FOCUS ON 3 MAJOR TAGETS BELOW: <br>
                                               • SUPPORT KOREAN COMPANIES’S PARTICIPATION IN VIETNAM <br>
                                               • PROMOTE VIETNAM AND KOREA ICT INDUSTRY SECTOR <br>
                                               • SUPPORT DEVELOPMENT OF A MARKET ECONOMY IN VIETNAM
                                           </p>
                                       </td>
                                   </tr>  <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">VISION</h5>
                                           <p class="description2">KICC IN HANOI BECOMES THE LARGEST AND MOST DEVELOPED FOREIGN COOPERATION ORGANIZATION IN VIETNAM</p>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">CORE VALUES</h5>
                                           <p class="description2">MAGINATION, CHALLENGE, INNOVATION, GLOBAL MIND</p>
                                       </td>
                                   </tr>
                               </table>
                           @elseif($locale === 'vi')
                               <div style="margin-left: auto;padding-left: 1em">
                                   <h5 class="title-korea-about">Trung tâm hợp tác công nghệ thông tin Hàn Quốc (KICC) tại HCMC.</h5>
                                   <p class="description2">
                                       Được thành lập vào ngày 23 tháng 5 năm 2019, Trung tâm Hỗ trợ CNTT Hồ Chí Minh đóng vai trò là trung tâm chiến lược cho Cơ quan Xúc tiến Công nghệ Thông tin và Truyền thông (NIPA) để thúc đẩy các cơ hội kinh doanh tích cực như một phương tiện kết nối Hàn Quốc và các nước ASEAN trong lĩnh vực CNTT. NIPA là một cơ quan chính phủ phi lợi nhuận và là thành viên của Bộ Khoa học, Công nghệ và Thông tin Hàn Quốc. Nipa thông qua sự tiến bộ của kỹ thuật CNTT và công nghiệp, hiện đang dẫn đầu kiến thức về cơ sở hạ tầng kinh tế xã hội và phát triển kinh tế quốc gia nhằm nâng cao khả năng cạnh tranh của toàn ngành công nghiệp.
                                   </p>
                               </div>
                               <table class="table table-about" style="margin-top: 2em">
                                   <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">Sứ mệnh</h5>
                                           <p class="description2">+ Phát triển nền Kinh tế năng động và Quốc gia thông qua ICT</p>
                                           <p class="description2">+ Liên kết và hỗ trợ các doanh nghiệp và các chuyên gia CNTT</p>
                                           <p class="description2">+ Xóa bỏ rào cản, hội tụ nền công nghiệp ICT.</p>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">Tầm nhìn</h5>
                                           <p class="description2">
                                               KICC muốn trở thành một tổ chức hợp tác nước ngoài lớn nhất và phát triển nhất ở Việt Nam.
                                           </p>
                                       </td>
                                   </tr>  <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">Giá trị cốt lõi</h5>
                                           <p class="description2">Trí tưởng tượng, Thách thức, Đổi mới, Tư duy toàn cầu</p>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td colspan="3">
                                           <h5  class="title-about">Mục đích</h5>
                                           <p class="description2">
                                               + Hỗ trợ các công ty Hàn Quốc vào hoạt động ở Việt Nam <br>
                                               + Thúc đẩy ngành công nghiệp ICT của Việt Nam và Hàn Quốc <br>
                                               + Hỗ trợ sự phát triển của thị trường kinh tế tại Việt Nam <br>
                                           </p>
                                       </td>
                                   </tr>
                               </table>
                           @else
                               <div style="margin-left: auto;padding-left: 1em">
                                   <h5 class="title-korea-about">Korea IT Cooperation Center (KICC) in HCMC</h5>
                                   <p class="description2">
                                       2019년 5월 23일에 설립된 호치민IT지원센터는 ICT 산업에서 한국과 아세안 국가들의
                                       매개체로서 적극적인 사업기회를 촉진시키기 위해 정보통신산업진흥원(이하NIPA)의
                                       전략적인 허브 역할을 제공하고 있습니다. NIPA는 비영리 정부기관이며, IT기업과 전문가들을
                                       지원하는 대한민국 과학기술정보통신부의 유관기관입니다. NIPA는 IT 기술 및 산업의 진보를
                                       통해 산업 전반의 경쟁력을 키우는 사회경제기반 지식 및 국가경제발전을 선도하고 있습니다.

                                   </p>
                               </div>
                               <table class="table table-about" style="margin-top: 2em">
                                   <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">MISSION</h5>
                                           <p class="description2">
                                               ICT를 통한 국가적, 경제적인 발전, IT기업들에 대한 지원, 진입장벽 제거, <br>
                                               ICT산업의 융합
                                           </p>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">VISION</h5>
                                           <p class="description2">
                                               KICC 하노이의 미래 비전은 “베트남에서 최고로 전문적이고 규모있는 <br>
                                               해외협력기구로의 성장”입니다.
                                           </p>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td colspan="3">
                                           <h5 class="title-about">PURPOSE</h5>
                                           <p class="description2">
                                               베트남 내에 진출하는 한국기업에 대한 지원 <br>
                                               베트남 및 한국ICT 산업분야 홍보 및 장려 <br>
                                               베트남 경제 성장 기여 <br>
                                           </p>
                                       </td>
                                   </tr>
                                   <tr>
                                        <td colspan="3">
                                            <h5 class="title-about">CORE VALUES</h5>
                                            <p class="description2">창의성, 도전정신, 혁신, 글로벌 마인드</p>
                                        </td>
                                    </tr>
                               </table>
                           @endif
                       </div>

                   </div>
               </section>
           </div>
       </div>
    </main>
@endsection
