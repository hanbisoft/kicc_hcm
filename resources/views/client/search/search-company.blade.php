@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form class="form-inline my-2 my-lg-0" action="{{route('searchCompany')}}" method="GET">
                        <input value="{{$key}}" class="form-control mr-sm-2" type="search" name="search" placeholder="내용을 입력해주세요" aria-label="Search">
                        <button class="btn" type="submit">조회</button>
                    </form>
                </div>
            </div>
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <section class="section-hb incubat-hb animation-element slide-left">
                    <div class="container">
                        <div class="heading">
                            <div class="heading-left">
                                <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                                <h3>{{ trans('messages.incubating') }}</h3>
                            </div>
                            <div class="heading-right">
                                <a href="{{route('company.incubating')}}" class="btn btn-hb btn-viewall">View all</a>
                            </div>
                        </div>
                        <div class="content">
                            <div class="incubat-inner owl-carousel">
                                @foreach($incubating as $value)
                                    <div class="incubat-box thumbnail animation-element scroll-zoom-out in-view">
                                        <a href="/company/{{$value->id}}"><div class="incubat-img new-img"><img src="{{asset($value->images)}}" alt=""></div></a>
                                        <div class="incubat-text caption">
                                            @if($locale === 'en')
                                                <h5><a href="/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                                <p class="description">{{$value->address}}</p>
                                            @else
                                                <h5><a href="/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                                <p class="description">{{$value->address}}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </section>
                <section class="section-hb incubat-hb animation-element slide-left">
                    <div class="container">
                        <div class="heading">
                            <div class="heading-left">
                                <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                                <h3>{{ trans('messages.b2b_matching') }}</h3>
                            </div>
                            <div class="heading-right">
                                <a href="{{route('company.b2b')}}" class="btn btn-hb btn-viewall">View all</a>
                            </div>
                        </div>
                        <div class="content">
                            <div class="incubat-inner owl-carousel">
                                @foreach($b2b_matching as $value)
                                    <div class="incubat-box thumbnail animation-element scroll-zoom-out in-view">
                                        <a href="/company/{{$value->id}}"><div class="incubat-img new-img"><img src="{{asset($value->images)}}" alt=""></div></a>
                                        <div class="incubat-text caption">
                                            @if($locale === 'en')
                                                <h5><a href="/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                                <p class="description">{{$value->address}}</p>
                                            @else
                                                <h5><a href="/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                                <p class="description">{{$value->address}}</p>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </section>
    </main>

@endsection