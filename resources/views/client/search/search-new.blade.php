@extends('master')
@section('content')
    <main class="main-hb">
        <div id="slider-nb" class="slider-nb section-hb"></div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form action="{{route('searchNew')}}" method="GET" class="form-inline my-2 my-lg-0">
                        <input name="search" class="form-control mr-sm-2" type="search" placeholder="{{ trans('messages.search_content') }}" aria-label="Search">
                        <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                    </form>
                </div>
            </div>
        </div>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt=""><h3>{{ trans('messages.news') }}</h3>
                    </div>
                    <div class="heading-right">
                        <a href="{{route('client.event.index')}}" class="btn btn-hb btn-viewall">{{ trans('messages.event') }}</a>
                    </div>
                </div>
                <div class="content">
                    <input type="hidden" id="locale" name="locale" value="{{$locale}}">
                    @foreach($new as $value)
                        @if($value->cate_id === 1)
                            <div class="row media" >
                                <div class="col-4 ">
                                    <a class="new-img" href="/new/{{$value->id}}/{{$value->slug}}">
                                        <img   src="{{asset($value->images)}}"
                                               class="rounded float-left img_new"/>
                                    </a>
                                </div>
                                <div class="col-8">
                                    @if($locale == 'en')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_en}}</a></h5>
                                        <p class="description">{{$value->description_en}}</p>
                                    @elseif($locale == 'vi')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_vi}}</a></h5>
                                        <p class="description">{{$value->description_vi}}</p>
                                    @elseif($locale == 'ko')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_ko}}</a></h5>
                                        <p class="description">{{$value->description_ko}}</p>
                                    @endif

                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>

        {{--//history--}}
        <section class="section-hb history-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.history') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <div class="history-inner owl-carousel">
                        @foreach($histories as $value)
                            <div class="history-box thumbnail animation-element scroll-zoom-out in-view">
                                <a href="/new/{{$value->id}}/{{$value->slug}}"><img src="{{asset($value->images)}}" alt=""></a>
                                <div class="history-text caption">
                                    @if($locale === 'en')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_en}}</a></h5>
                                        <p>{{str_limit($value->description_en,100)}}</p>
                                    @elseif($locale === 'vi')
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_vi}}</a></h5>
                                        <p>{{str_limit($value->description_vi,100)}}</p>
                                    @else
                                        <h5><a href="/new/{{$value->id}}/{{$value->slug}}">{{$value->title_ko}}</a></h5>
                                        <p>{{str_limit($value->description_ko,100)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
