@extends('master')
@section('content')
    <main class="main-hb">
        <h5 id="wait"></h5>
        <div id="slider-nb" class="slider-nb">
            <div class="slider-inner owl-carousel">
                <div class="slider-item">
                    <img src="banner/ok4-min.jpg" alt="">
                    <div class="box-text">
                        <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                        <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                    </div>
                </div>
                <div class="slider-item">
                    <img src="banner/ok-min.jpg" alt="">
                    <div class="box-text">
                        <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                        <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                    </div>
                </div>
                <div class="slider-item">
                    <img src="banner/ok2-min.jpg" alt="">
                    <div class="box-text">
                        <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                        <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                    </div>
                </div>
                <div class="slider-item">
                    <img src="banner/ok3-min.jpg" alt="">
                    <div class="box-text">
                        <h2 class="animated fadeInLeftBig">A BRIDGE BETWEEN INNOVATIVE TECHNOLOGY</h2>
                        <h3 class="animated fadeInRightBig">AND INDUSTRY IN VIETNAM</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="search-hb section-hb animation-element slide-left">
            <div class="container">
                <div class="search-inner">
                    <form class="form-inline my-2 my-lg-0" action="{{route('search')}}" method="GET">
                        <input value="{{$key}}" name="search" class="form-control mr-sm-2" type="search"
                               placeholder="{{ trans('messages.search_content') }}"
                               aria-label="Search">
                        <input  type="hidden" name="locale" id="locale" value="{{$locale}}">
                        <button class="btn" type="submit">{{ trans('messages.search') }}</button>
                    </form>


                </div>
            </div>
        </div>
        <section class="section-hb new-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="images/icon-chuan1.jpg" alt="">
                        <h3>{{ trans('messages.news') }} & {{ trans('messages.event') }}</h3>
                    </div>
                    <div class="heading-right">
                        <a href="{{route('new.index')}}" class="btn btn-hb btn-new">{{ trans('messages.news') }}</a>
                        <a href="{{route('client.event.index')}}"
                           class="btn btn-hb btn-event">{{ trans('messages.event') }}</a>
                    </div>
                </div>
                <div class="content">
                    <div class="new-inner owl-carousel">
                        @foreach($event as $value)
                            <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                                <div class="new-img" style="max-height: 233.525px" onclick="showEvent({{$value->id}})">
                                    <img
                                            src="{{asset($value->images)}}" alt="" class="img-fluid">
                                </div>
                                <div class="new-box">
                                    <div class="new-time">
                                        <p>{{$value->day}}</p>
                                        <span>{{$value->month}}</span>
                                    </div>
                                    <div class="new-title ">
                                        @if($locale === 'en')
                                            <h5><a>{{$value->title_en}}</a></h5>
                                        @elseif($locale === 'vi')
                                            <h5><a>{{$value->title_vi}}</a></h5>
                                        @else
                                            <h5><a>{{$value->title_ko}}</a></h5>
                                        @endif
                                        <p><i class="fa fa-clock-o"></i>{{$value->time_start}}
                                            - {{$value->time_end}}
                                        </p>
                                        <p><i class="fas fa fa-map-marker"></i>Keangnam Hanoi</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="section-hb incubat-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.incubating') }}</h3>
                    </div>
                    <div class="heading-right">
                        <a href="{{route('client.company.index')}}"
                           class="btn btn-hb btn-viewall">{{ trans('messages.view_all') }}</a>
                    </div>
                </div>
                <div class="content">
                    <div class="incubat-inner owl-carousel">
                        @foreach($incubating as $value)
                            <div class="incubat-box thumbnail animation-element scroll-zoom-out in-view">
                                <a href="/company/{{$value->id}}">
                                    <div class="match-img new-img" style="max-height: 233.525px"><img
                                                src="{{asset($value->images)}}" alt=""></div>
                                </a>
                                <div class="incubat-text caption">
                                    @if($locale === 'en')
                                        <h5><a href="/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                        <p>{{str_limit($value->address,30)}}</p>
                                    @elseif($locale === 'vi')
                                        <h5><a href="/company/{{$value->id}}">{{$value->company_name_vi}}</a></h5>
                                        <p>{{str_limit($value->address,30)}}</p>
                                    @else
                                        <h5><a href="/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                        <p>{{str_limit($value->address,30)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="section-hb showroom-hb animation-element slide-left showhome">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/iconchuan2.jpg')}}" alt="">
                        <h3 class="text-footer">{{ trans('messages.showroom') }}</h3>
                    </div>
                    <div class="heading-right">
                        <a href="{{route('client.showroom.index')}}"
                           class="btn btn-hb btn-viewall btn-showroom"
                           style="border: 1px solid">{{ trans('messages.view_all') }}</a>
                    </div>
                </div>
                <div class="content">
                    <div class="showroom-inner owl-carousel">
                        @foreach($showrom as $value)
                            <div class="showroom-box thumbnail animation-element scroll-zoom-out in-view">
                                <a class="new-img" href="/showroom/{{$value->id}}"><img src="{{asset($value->images)}}"
                                                                                        alt=""></a>
                                <div class="showroom-text caption">
                                    @if($locale === 'en')
                                        <h5><a class="text-showroom"
                                               href="/showroom/{{$value->id}}">{{$value->title_en}}</a></h5>
                                        <p class="text-footer">{{str_limit($value->description_en,100)}}</p>
                                    @elseif($locale === 'vi')
                                        <h5><a class="text-showroom"
                                               href="/showroom/{{$value->id}}">{{$value->title_vi}}</a></h5>
                                        <p class="text-footer">{{str_limit($value->description_vi,100)}}</p>
                                    @else
                                        <h5><a class="text-showroom"
                                               href="/showroom/{{$value->id}}">{{$value->title_ko}}</a></h5>
                                        <p class="text-footer">{{str_limit($value->description_ko,100)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </section>
        <section class="section-hb match-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.b2b_matching') }}</h3>
                    </div>
                    <div class="heading-right">
                        {{--<input class="btn btn-hb btn-search" value="Search" type="text">--}}
                        <a href="{{route('company.b2b')}}"
                           class="btn btn-hb btn-viewall">{{ trans('messages.view_all') }}</a>
                    </div>
                </div>
                <div class="content">
                    <div class="match-inner owl-carousel">
                        @foreach($b2b as $value)
                            <div class="match-box thumbnail animation-element scroll-zoom-out in-view">
                                <a href="/company/{{$value->id}}">
                                    <div class="match-img new-img" style="max-height: 233.525px"><img
                                                src="{{asset($value->images)}}" alt=""></div>
                                </a>
                                <div class="match-text caption">
                                    @if($locale === 'en')
                                        <h5><a href="/company/{{$value->id}}">{{$value->company_name_en}}</a></h5>
                                        <p>{{str_limit($value->address,100)}}</p>
                                    @elseif($locale === 'vi')
                                        <h5><a href="/company/{{$value->id}}">{{$value->company_name_vi}}</a></h5>
                                        <p>{{str_limit($value->address,100)}}</p>
                                    @else
                                        <h5><a href="/company/{{$value->id}}">{{$value->company_name_ko}}</a></h5>
                                        <p>{{str_limit($value->address,100)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="section-hb history-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.history') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <div class="history-inner owl-carousel">
                        @foreach($history as $value)
                            <div class="history-box thumbnail animation-element scroll-zoom-out in-view">
                                <div class="new-img  " onclick="show({{$value->id}})">
                                    <img src="{{asset($value->images)}}" alt="">
                                </div>
                                <div class="history-text caption">
                                    @if($locale === 'en')
                                        <h5><a>{{$value->title_en}}</a></h5>
                                        <p>{{str_limit($value->description_en,100)}}</p>
                                    @elseif($locale === 'vi')
                                        <h5><a>{{$value->title_vi}}</a></h5>
                                        <p>{{str_limit($value->description_vi,100)}}</p>
                                    @else
                                        <h5><a>{{$value->title_ko}}</a></h5>
                                        <p>{{str_limit($value->description_ko,100)}}</p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="section-hb share-hb animation-element slide-left">
            <div class="container">
                <div class="heading">
                    <div class="heading-left">
                        <img src="{{asset('images/icon-chuan1.jpg')}}" alt="">
                        <h3>{{ trans('messages.share_room') }}</h3>
                    </div>
                </div>
                <div class="content">
                    <div class="share-inner owl-carousel">
                        <div class="share-box animation-element scroll-zoom-out in-view">
                            <div class="share-img"><img src="images/lage.jpg" alt=""></div>
                            <div class="share-text">
                                <h5 class="text-book"><a href="">Large meeting room</a></h5>
                                <a href="/book/1" class="btn btn_book">Book</a>
                            </div>
                        </div>
                        <div class="share-box animation-element scroll-zoom-out in-view">
                            <div class="share-img">
                                <img src="images/smal-office.jpg" alt="">
                            </div>
                            <div class="share-text">
                                <h5 class="text-book"><a href="">Small Meeting room </a></h5>
                                <a href="/book/2" class="btn btn_book">Book</a>
                            </div>
                        </div>
                        <div class="share-box animation-element scroll-zoom-out in-view">
                            <div class="share-img">
                                <img src="images/co.jpg" alt="">
                            </div>
                            <div class="share-text">
                                <h5 class="text-book"><a href="">Open Co-Working Space</a></h5>
                                <a href="/book/3" class="btn btn_book">Book</a>
                            </div>
                        </div>
                    </div>
                    <div class="brand-hb owl-carousel">
                        <div class="brand-item animation-element scroll-zoom-out in-view">
                            <img src="images/sharelogo1.jpg" alt="">
                        </div>
                        <div class="brand-item animation-element scroll-zoom-out in-view">
                            <img src="images/sharelogo2.jpg" alt="">
                        </div>
                        <div class="brand-item animation-element scroll-zoom-out in-view">
                            <img src="images/sharelogo3.jpg" alt="">
                        </div>
                        <div class="brand-item animation-element scroll-zoom-out in-view">
                            <img src="images/sharelogo4.jpg" alt="">
                        </div>
                        <div class="brand-item animation-element scroll-zoom-out in-view">
                            <img src="images/sharelogo2.jpg" alt="">
                        </div>
                        <div class="brand-item animation-element scroll-zoom-out in-view">
                            <img src="images/sharelogo3.jpg" alt="">
                        </div>
                        <div class="brand-item animation-element scroll-zoom-out in-view">
                            <img src="images/sharelogo4.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    {{--//modal history--}}
    <div class="popup-product">
        <div id="new" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="social-new">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FKICC.vn%2F%3Fepa%3DSEARCH_BOX&width=134&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId"
                                width="134" height="46" style="border:none;overflow:hidden" scrolling="no"
                                frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                        <span id="excel"></span>
                        <span id="pdf"></span>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img">
                                <div id="img"></div>
                            </div>
                            <div class="new-box">
                                <div class="new-title">
                                    <h6 id="title"></h6>
                                    <div id="content"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    {{--//modal event--}}
    <div class="popup-product">
        <div id="event-popup" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header new-title">
                        <div class="new-time" data-toggle="modal" data-target="#event-popup">
                            <p id="time_start"></p>
                            <span id="month"></span>
                        </div>
                        <div class="new-title ">
                            <div class="popup-product section-hb">
                                <h5 id="event-title"></h5>
                            </div>
                            <p id="hour"></p>
                            <p><i class="fas fa fa-map-marker"></i>Keangnam Hanoi</p>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="new-item thumbnail animation-element scroll-zoom-out in-view">
                            <div class="new-img">
                                <div id="img-event"></div>
                            </div>
                            <div class="new-box">
                                <div class="new-title">
                                    <h6 id="title-event"></h6>
                                    <div class="description" id="content-event"></div>
                                </div>
                            </div>
                            <div class="box-place">
                                <div class="place-item">
                                    <i class="fa fa-clock-o"></i>
                                    <p>Time: <br><span id="hour2"></span></p>
                                </div>
                                <div class="place-item">
                                    <i class="fas fa fa-map-marker"></i>
                                    <p>Place: <br><span>Keangnam Hanoi</span></p>
                                </div>
                                <div class="place-item">
                                    <div class="social-popup">
                                        <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    </div>
                                    </a>
                                    <a href="">share</a>
                                </div>
                                <div class="place-item">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <a href="">Add to your calendar </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
    </div>
    <script>
        function show(t) {
            var language = document.getElementById("locale").value,
                getTitle = "",
                getUrlImage = "",
                getContent = "",
                downloadExcel = "";
            downloadPdf = "";
            $.ajax({
                url: "{{ route('api.new') }}",
                beforeSend: function () {
                    $("#wait").show()
                }, complete: function () {
                    $("#wait").hide()
                }, type: "get",
                data: {locale: language, id: t},
                dateType: "text", success: function (t) {
                    getTitle += t.title;
                    getUrlImage += "<img class='img-fluid' src='" + t.images + "'>";
                    downloadExcel += " <a  download href='" + t.file_excel + "' class='btn btn-hb btn-download'>Download Excel</a>";
                    downloadPdf += " <a  download href='" + t.file_pdf + "' class='btn btn-hb btn-download'>Dowload Pdf</a>";
                    getContent += t.content;
                    $("#exampleModalLongTitle").html(getTitle);
                    $("#title").html(getTitle);
                    $("#img").html(getUrlImage);
                    if(t.file_excel != null) {
                        $("#excel").html(downloadExcel);
                    }
                    if(t.file_pdf != null) {
                        $("#pdf").html(downloadPdf);
                    }
                    $("#content").html("<p class='description_modal'>" + getContent + "</p>");
                    $("#new").modal("show");
                }
            })
        }

        function showEvent(t) {
            var e = document.getElementById("locale").value,
                getTitle = "";
            getUrlImage = "";
            getContent = "";
            download = "";
            get_time_start = "";
            get_time_end = "";
            get_month = "";
            getDay = "";
            $.ajax({
                url: "{{ route('api.new') }}", beforeSend: function () {
                    $("#wait").show()
                }, complete: function () {
                    $("#wait").hide()
                }, type: "get", data: {locale: e, id: t}, dateType: "text", success: function (t) {
                    getTitle += t.title,
                        getUrlImage += "<img   class='img-fluid' src='" + t.images + "'>",
                        getContent += t.content,
                        getDay += t.day,
                        get_month += t.month,
                        get_time_start += t.time_start,
                        get_time_end += t.time_end,
                        $("#event-title").html(getTitle);
                    $("#titleEvent").html(getTitle);
                    $("#img-event").html(getUrlImage);
                    $("#content-event").html(getContent),
                        $("#time_start").html(getDay);
                    $("#month").html(get_month);
                    $("#hour").html("<i class='fa fa-clock-o'></i>" + get_time_start + " - " + get_time_end);
                    $("#hour2").html("<i class='fa fa-clock-o'></i>" + get_time_start + " - " + get_time_end);
                    $("#event-popup").modal("show")
                }
            })
        }
    </script>

@endsection
