<?php

return [
    'event' => 'Event',
    'news' => 'News',
    'incubating' => 'Incubating',
    'showroom' => 'Showroom',
    'b2b_matching' => 'B2B Matching',
    'history' => 'Project',
    'share_room' => 'Networking',
    'company_infomation'=> 'Company Infomation',
    'company' =>'Company',
    'support'=>'Support',
    'about'=>'About Us ',
    'login'=>'Login',
    'register'=>'Register',
    'view_all'=>'View All',
    'search'=>'Search',
    'key_search'=>'Please enter your details',
    'contact' =>'Contact us',
    'asean' => 'ASEAN',
    'book_shareroom' => 'Book Shareroom Free',
    'new_event'=> 'News & Event',
    'search'=>'Search',
    'search_content'=> 'Please enter your details',
    'title_kicc_hcm' => "Facilities",
    'title_logo' => "In-House & membership company",

    'title_product_1' => "Center",
    'title_product_2' => "Meeting room",
    'title_product_3' => "Coworking space",
    'title_product_4' => "Lobby",
    'title_product_5' => "Office",
    'title_product_6' => "Lounge/Multi-function printer",
    "registration_condition" => "Registration",
    "market_infomation" => "Market Information",

    //  menu   
    "menu_1" => "ABOUT US",
    "menu_2" => "FACILITIES",
    "menu_3" => "NETWORKING",
    "menu_4" => "REGISTRATION",
    "menu_5" => "MARKET INFORMATION",
    "menu_6" => "CONTACT US",
    
 
];
