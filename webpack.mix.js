let mix = require('laravel-mix');

mix.scripts([
    'public/js/bootstrap.min.js',
], 'public/dist/vendor-bootstrap.js')

mix.scripts([
    'public/js/jquery.min.js',
], 'public/dist/vendor-jquery.js')

mix.js([
    'public/js/main.js',
    'public/js/owl.carousel.min.js'
], 'public/dist/custom.js')

mix.styles([
    'public/css/bootstrap.min.css',
], 'public/dist/vendor.css')

mix.styles([
    'public/css/style.css',
    'public/css/owl.carousel.min.css',
], 'public/dist/theme.css')

