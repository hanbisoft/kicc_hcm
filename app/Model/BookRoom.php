<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BookRoom extends Model
{
    protected $table = 'book_rooms';
    protected $fillable = [
        'id',
        'start_date',
        'end_date',
        'user_id',
        'status',
        'room_id',
        'full_name',
        'company_name',
        'email',
        'phone',
        'qty',
        'note',
    ];
}
