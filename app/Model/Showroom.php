<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Showroom extends Model
{
    protected $fillable = [
        'title_en',
        'title_vi',
        'title_ko',
        'description_en',
        'description_vi',
        'description_ko',
        'content_en',
        'content_vi',
        'content_ko',
        'images',
        'images1',
        'images2',
        'slug',
        'file_excel',
        'file_pdf',
        'tel',
        'email',
        'address',
    ];
}
