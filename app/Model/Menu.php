<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $fillable = [
        'id',
        'name_en',
        'name_vi',
        'name_ko',
        'status',
        'url',
    ];
}
