<?php

namespace App\Imports;

use App\Model\Post;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Config;

class PostImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $slug = str_slug($row['title_en']);
        return new Post([
            'title_en' => $row['title_en'],
            'title_vi' => $row['title_vi'],
            'title_ko' => $row['title_ko'],
            'content_en' => $row['content_en'],
            'content_vi' => $row['content_vi'],
            'content_ko' => $row['content_ko'],
            'description_en' => $row['description_en'],
            'description_vi' => $row['description_vi'],
            'description_ko' => $row['description_ko'],
            'cate_id' => $row['cate_id'],
            'month' => $row['month'],
            'time_start' => $row['time_start'],
            'time_end' => $row['time_end'],
            'day' => $row['day'],
            'slug' => $slug,
        ]);
    }

    //function quy dinh dong import
    public function headingRow(): int
    {
        return 1;
    }
}
