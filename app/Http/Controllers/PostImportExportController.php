<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\PostExport;
use App\Imports\PostImport;
use Maatwebsite\Excel\Facades\Excel;
use Config;

class PostImportExportController extends Controller
{
    public function importExportView()
    {
        return view('import');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function export()
    {
        return Excel::download(new PostExport, 'posts.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function importNew()
    {
        Excel::import(new PostImport(), request()->file('file'));

        return back();
    }

    public function importHistory()
    {
        Excel::import(new PostImport(), request()->file('file'));
        return back();
    }


}
