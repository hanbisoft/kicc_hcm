<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Repositories\Eloquent\PostEloquentRepository;
use App\Model\Post;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use App;
use Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

class AdminPostController extends Controller
{
    protected $postRepository;

    function __construct(
        PostEloquentRepository $postRepository
    )
    {
        // parent::__construct();
        $this->postRepository = $postRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $NEW = 1;
    private $EVENT = 2;
    private $HISTORY = 3;

    public function index()
    {
        return view('backend.post.list');
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function showDataNew()
    {
        $sql = DB::table('posts')->select(['id', 'title_en', 'title_ko', 'title_vi',])->where('cate_id', 1)->get();
        $token = Session::token();
        return Datatables::of($sql)
            ->addColumn('action', function ($post) use ($token) {
                return '
                <a href="/admin/post/' . $post->id . '/edit" class="btn btn-xs btn-primary btn_datatable"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <a href="javascript:void(0);" onclick="return confirmDelete(' . $post->id . ')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                 <form action="/admin/post/' . $post->id . '" method="post" id="frm_delete_' . $post->id . '">
                    <input type="hidden" name="_token" id="csrf-token" value="' . $token . '" />
                    <input type="hidden" name="_method" value="DELETE">
                </form>';
            })
            ->make(true);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function showDataHistory()
    {
        $sql = DB::table('posts')->select(['id', 'title_en', 'title_ko', 'title_vi',])->where('cate_id', 3)->get();
        $token = Session::token();
        return Datatables::of($sql)
            ->addColumn('action', function ($post) use ($token) {
                return '
                <a href="/admin/post/' . $post->id . '/edit" class="btn btn-xs btn-primary btn_datatable"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <a href="javascript:void(0);" onclick="return confirmDelete(' . $post->id . ')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                 <form action="/admin/post/' . $post->id . '" method="post" id="frm_delete_' . $post->id . '">
                    <input type="hidden" name="_token" id="csrf-token" value="' . $token . '" />
                    <input type="hidden" name="_method" value="DELETE">
                </form>';
            })
            ->make(true);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function showDataEvent()
    {
        $sql = DB::table('posts')->select(['id', 'title_en', 'title_ko', 'title_vi',])->where('cate_id', 2)->get();
        $token = Session::token();
        return Datatables::of($sql)
            ->addColumn('action', function ($post) use ($token) {
                return '
                <a href="/admin/event/' . $post->id . '/edit" class="btn btn-xs btn-primary btn_datatable"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <a href="javascript:void(0);" onclick="return confirmDelete(' . $post->id . ')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                 <form action="/admin/post/' . $post->id . '" method="post" id="frm_delete_' . $post->id . '">
                    <input type="hidden" name="_token" id="csrf-token" value="' . $token . '" />
                    <input type="hidden" name="_method" value="DELETE">
                </form>';
            })
            ->make(true);
    }

    //list event

    public function listEvent()
    {
        return view('backend.post.list-event');
    }

    //list history

    public function listHistory()
    {

        return view('backend.post.list-history');
    }


    public function createEvent()
    {
        return view('backend.post.create_event');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.post.create');
    }

    public function updateEvent($id)
    {
        $event = $this->postRepository->find($id);
//        return $event;
        return view('backend.post.edit_event', compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request->all();
        if ($request->hasFile('img')) {
            $image = $request->file('img');
            //rename
            $filename = str_random(4) . $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(320, 200);
            $image_resize->save(public_path('uploads/' . $filename));
            $urlFile = 'uploads/' . $filename;
        }

        //excel
        if ($request->hasFile('file_excel')) {
            $excel = $request->file('file_excel')->store('documents');
            $url_excel = $excel;
        } else {
            $url_excel = "";
        }
        //pdf
        if ($request->hasFile('file_pdf')) {
            $pdf = $request->file('file_pdf')->store('documents');
            $url_pdf = $pdf;
        } else {
            $url_pdf = "";
        }

        $slug = str_slug($request->get('title_en'));
        $post = new Post;
        $post->title_en = $request->get('title_en');
        $post->title_vi = $request->get('title_vi');
        $post->title_ko = $request->get('title_ko');
        $post->description_en = $request->get('description_en');
        $post->description_vi = $request->get('description_vi');
        $post->description_ko = $request->get('description_ko');
        $post->content_en = $request->get('content_en');
        $post->content_vi = $request->get('content_vi');
        $post->content_ko = $request->get('content_ko');
        $post->images = $urlFile;
        $post->slug = $slug;
        $post->cate_id = $request->get('cate_id');
        $post->day = $request->get('day');
        $post->month = $request->get('month');
        $post->year = $request->get('year');
        $post->time_start = $request->get('time_start');
        $post->time_end = $request->get('time_end');
        $post->file_excel = $url_excel;
        $post->file_pdf = $url_pdf;
        $post->created_at = $request->get('created_at');
        $post->end_day = $request->get('end_day');
        $post->place = $request->get('place');

        $post->save();
        return redirect()->back()->with('message', 'Create successful!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new = $this->postRepository->find($id);
        return view('backend.post.edit', compact('new'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if ($request->hasFile('img')) {
            $image = $request->file('img');
            $filename = str_random(4) . $image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(320, 200);
            $image_resize->save(public_path('uploads/' . $filename));
            $urlFile = 'uploads/' . $filename;
        } else {
            $urlFile = $post->images;
        }
        //excel
        if ($request->hasFile('file_excel')) {
            $excel = $request->file('file_excel')->store('documents');
            $url_excel = $excel;
        } else {
            $url_excel = $post->file_excel;
        }
        //pdf
        if ($request->hasFile('file_pdf')) {
            $pdf = $request->file('file_pdf')->store('documents');
            $url_pdf = $pdf;
        } else {
            $url_pdf = $post->file_pdf;
        }

        $slug = str_slug($request->get('title_en'));
        $post->title_en = $request->get('title_en');
        $post->title_vi = $request->get('title_vi');
        $post->title_ko = $request->get('title_ko');
        $post->description_en = $request->get('description_en');
        $post->description_vi = $request->get('description_vi');
        $post->description_ko = $request->get('description_ko');
        $post->content_en = $request->get('content_en');
        $post->content_vi = $request->get('content_vi');
        $post->content_ko = $request->get('content_ko');
        $post->images = $urlFile;
        $post->slug = $slug;
        $post->cate_id = $request->get('cate_id');
        $post->day = $request->get('day');
        $post->month = $request->get('month');
        $post->year = $request->get('year');
        $post->time_start = $request->get('time_start');
        $post->time_end = $request->get('time_end');
        $post->file_excel = $url_excel;
        $post->file_pdf = $url_pdf;
        $post->created_at = $request->get('created_at');
        $post->end_day = $request->get('end_day');
        $post->place = $request->get('place');
        $post->save();

        return redirect()->back()->with('message', 'Update successful!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->postRepository->delete($id);
        return redirect()->back()->with('message', 'Delete successful!');
    }
}
