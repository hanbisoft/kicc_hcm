<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Model\BookRoom;
use Session;
use App;
use Config;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\User;
use DateTime;
use DateTimeZone;
use Auth;
use Mail;
class AdminBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.book.index');
    }

    public function showListBook()
    {
        $dateTen = Carbon::now()->addDay(10);
        $date_now = Carbon::now()->subDays(10);
        $s = "select b.*,CASE WHEN b.room_id = 1 THEN \"Lage meeting room\"WHEN b.room_id = 2 THEN \"Small meeting room\"WHEN b.room_id = 3 THEN \"Open Co-Working Space\"END AS room_name FROM book_rooms b ORDER BY b.created_at DESC";
//        echo $s;die;
        $sql = DB::select($s);
        $token = Session::token();
        return Datatables::of($sql)
            ->addColumn('action', function ($book) use ($token) {
                return '
                <a href="javascript:void(0);" onclick="return confirmDelete(' . $book->id . ')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
                 <form action="/admin/admin-book/' . $book->id . '" method="post" id="frm_delete_' . $book->id . '">
                    <input type="hidden" name="_token" id="csrf-token" value="' . $token . '" />
                    <input type="hidden" name="_method" value="DELETE">
                </form>';
            })
            ->make(true);
    }

    public function destroy($id)
    {
        $book = BookRoom::find($id);
        $book->delete();
        $user = User::find($book->user_id);
        $email = $user->email;

          //TODO GUI MAIL THONG BAO
          Mail::send('mail.cancel', array(
            'email' => $user->email,
            'room' => $id,
            'time_start' => $book->start_date,
            'time_end' => $book->end_date,
            'id' => $book->id

        ), function ($message) use ($email) {
            $message->to($email, 'KICC -Korea IT Cooperation Center in Hanoi')->subject('Cancel the meeting booking!');
        });
        return redirect()->back();
    }

    public function show()
    {
    }
}
