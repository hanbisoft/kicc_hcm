<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App;
class LanguageController extends Controller
{

    /**
     * @param Request $request
     * @param $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public  function changeLanguage( $locale) {
        Session::put('locale', $locale);
        App::setLocale(Session::get('locale'));
      return redirect()->back();
    }
}
