<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Model\BookRoom;
use App\User;
use DateTime;
use DateTimeZone;
use Auth;
use Mail;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $ACTIVE = 1;

    public function apiShowListBook(Request $request)
    {
        $id = $request->get('room_id');
        if (!$id || $id === 4) {
            $book = BookRoom::all();
        } else {
            $book = BookRoom::where('room_id', $id)->get();
        }

        $arr_data = [];
        $background = '';
        foreach ($book as $value) {
            if ($value->room_id === 1) {
                $background = '#0069d9'; //xanh da troi
            } elseif ($value->room_id === 2) {
                $background = '#218838';  //xanh la ma
            } elseif ($value->room_id === 3) {
                $background = '#c82333'; //do
            }
            $data = [
                'id' => $value->id,
                'title' => $value->company_name,
                'start' => $value->start_date,
                'end' => $value->end_date,
                'room' => $value->id,
                'backgroundColor' => $background,
                'eventColor' => '#ffffff',
                'className' => 'info',
            ];
            array_push($arr_data, $data);
        }
        return response()->json($arr_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($getid)
    {
        $id = $getid;
        return view('client.book.index', compact('id'));
    }

    public function show1()
    {
        $id = 1;
        return view('client.book.index2', compact('id'));
    }

    public function show2()
    {
        $id = 2;
        return view('client.book.index3', compact('id'));
    }

    public function show3()
    {
        $id = 3;
        return view('client.book.index4', compact('id'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function book(Request $request)
    {
        $space = ' ';
        $start_date = $request->get('start_date') . $space . $request->get('time_start');
        $end_date = $request->get('end_date') . $space . $request->get('time_end');
        $check = BookRoom::where('start_date', $start_date)->where('room_id', $request->get('room_id'))->count();

        if ($check === 1) {
            return response()->json([
                'code' => 400,
                'mess' => "This time has been booked, please choose another time."
            ]);
        } else {
            $user = User::find($request->get('user_id'));
            $book = new BookRoom;
            $book->start_date = $start_date;
            $book->end_date = $end_date;
            $book->user_id = $user->id;
            $book->phone = $user->phone;
            $book->email = $user->email;
            $book->full_name = $user->name;
            $book->qty = $request->get('qty');
            $book->status = $this->ACTIVE;
            $book->room_id = $request->get('room_id');
            $book->note = $request->get('note');
            $book->company_name = $request->get('company_name');
            $book->save();
            $email = $user->email;
            //TODO GUI MAIL THONG BAO
            Mail::send('mail.mail', array(
                'name' => $user->name,
                'email' => $user->email,
                'room' => $request->get('room_id'),
                'time_start' => $start_date,
                'time_end' => $end_date,
                'id' => $book->id

            ), function ($message) use ($email) {
                $message->to($email, 'KICC -Korea IT Cooperation Center in Hanoi')->subject('Successful meeting booking!');
            });
            return response()->json([
                'code' => 200,
                'mess' => "Successful meeting booking, Please check email details!"
            ]);
        }
    }

    public function confirm()
    {
        $book = BookRoom::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
        return view('client.book.message', compact('book'));
    }


    public function showBackEnd()
    {
        $book = BookRoom::all();
        $arr_data = [];
        $background = '';
        $room = '';
        foreach ($book as $value) {
            if ($value->room_id === 1) {
                $background = '#0069d9'; //xanh da troi
                $room = "Large meeting room";
            } elseif ($value->room_id === 2) {
                $background = '#218838';  //xanh la ma
                $room = "Small Meeting room";
            } elseif ($value->room_id === 3) {
                $background = '#c82333'; //do
                $room = "Open Co-Working Space";
            }
            $data = [
                'id' => $value->id,
                'title' => "Company name: " . $value->company_name . ". " . " Email: " . $value->email . "." . " Room: " . $room . "." . " Total people: " . $value->qty,
                'start' => $value->start_date,
                'end' => $value->end_date,
                'room' => $value->id,
                'backgroundColor' => $background,
                'eventColor' => '#ffffff',
                'className' => 'info',
            ];
            array_push($arr_data, $data);
        }
        return response()->json($arr_data);
    }


    //cancer booking
    public function deleteBooking($id)
    {
        $book = BookRoom::find($id);
        $book->delete();
        return redirect()->back();
    }

    //function get book history user
    public function bookHistory()
    {
        $user = Auth::user()->id;
        $img = User::find($user);
        $images = $img->images;
        $mytime = Carbon::now();
        $book = BookRoom::where('user_id', $user)->orderByRaw('created_at DESC')->paginate(5);;
        return view('client.user.index', compact(['book', 'images', 'mytime']));
    }


    public function apiShowListBookPDF(Request $request)
    {
        $id = $request->get('room_id');
        if (!$id) {
            $book = BookRoom::all();
        } else {
            $book = BookRoom::where('room_id', $id)->get();
        }

        $arr_data = [];
        $background = '';
        foreach ($book as $value) {
            if ($value->room_id === 1) {
                $background = '#0069d9'; //xanh da troi
            } elseif ($value->room_id === 2) {
                $background = '#218838';  //xanh la ma
            } elseif ($value->room_id === 3) {
                $background = '#c82333'; //do
            }
            $room_name = '';
            switch ($value->room_id)
            {
                case 1:
                    $room_name = "Large";
                break;
                case 2:
                    $room_name = "Small";
                break;
                case 3:
                    $room_name = "Open";
                break;
            }
            $user = User::find($value->user_id);
            $data = [
                'id' => $value->id,
                'text' =>  $value->company_name. " - ". $user->name ,
                'start' => $value->start_date,
                'end_chua_cong' => $value->end_date,
                'end' => date('Y-m-d H:i:s',strtotime($value->end_date)+ 3600),
                'room' => $value->room_id,
                'backgroundColor' => $background,
                'eventColor' => '#ffffff',
                'className' => 'info',
                'barColor' => $background
            ];
            array_push($arr_data, $data);
        }
        return response()->json($arr_data);
    }

}
