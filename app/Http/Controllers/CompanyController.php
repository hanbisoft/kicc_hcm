<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Repositories\Eloquent\CompanyEloquentRepository;
use  App\Repositories\Eloquent\PostEloquentRepository;
use  App\Repositories\Eloquent\ShowroomEloquentRepository;
use Session;
use App;
use Config;
use Illuminate\Support\Facades\DB;
use App\Model\Post;
use App\Model\Company;

class CompanyController extends Controller
{
    protected $companyRepository;
    protected $postRepository;
    protected $roomRepository;

    function __construct(
        CompanyEloquentRepository $companyRepository,
        PostEloquentRepository $postRepository,
        ShowroomEloquentRepository $roomRepository
    )
    {
        // parent::__construct();
        $this->companyRepository = $companyRepository;
        $this->postRepository = $postRepository;
        $this->roomRepository = $roomRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $b = "select * from companies where cate_id = 3 ORDER BY RAND() limit 10 ";
        $b2b_matching = DB::select(DB::raw($b));

        $i = "select * from companies where cate_id = 2 ORDER BY RAND() limit 10";
        $incubating = DB::select(DB::raw($i));

        $h = "select * from posts where cate_id = 3 ORDER BY RAND() limit 10";
        $history = DB::select(DB::raw($h));

        $e = "select * from posts p where p.cate_id  != 3   ORDER BY p.created_at DESC limit 10";
        $event = DB::select(DB::raw($e));
        // return $event;
        $s = "select * from showrooms ORDER BY RAND() limit 10";
        $showrom = DB::select(DB::raw($s));
        return view('client', compact(['b2b_matching', 'incubating', 'showrom', 'locale', 'event', 'history']));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showList()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $b = "
        SELECT *
        FROM companies c
        WHERE c.cate_id = 3
        ORDER BY created_at DESC limit 10";
        $b2b_matching = DB::select(DB::raw($b));

        $i = "SELECT *
        FROM companies c
        WHERE c.cate_id = 2
        ORDER BY created_at DESC limit 10";
        $incubating = DB::select(DB::raw($i));

        return view('client.company.company', compact(['incubating', 'b2b_matching', 'locale']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $company = $this->companyRepository->find($id);
        // return $company;
        return view('client.company.detail', compact(['company', 'locale']));
    }

    public function showIncubating()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $incubating = Company::where('cate_id', 2)->paginate('10');
        return view('client.company.incubating', compact(['incubating', 'locale']));
    }

    public function showCompanyAll()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $company = Company::where('cate_id', 1)->paginate('10');
        return view('client.company.company-all', compact(['company', 'locale']));
    }

    public function showB2b()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $b2b = Company::where('cate_id', 3)->paginate('10');
        return view('client.company.b2b', compact(['b2b', 'locale']));
    }

}
