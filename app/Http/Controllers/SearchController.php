<?php

namespace App\Http\Controllers;
use Session;
use App;
use Config;
use Illuminate\Http\Request;
use App\Model\Post;
use App\Model\Company;
use App\Model\Showroom;
use Illuminate\Support\Facades\DB;
class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }

        $key = $request->get('search');
        $b = "select c.* from companies c  where cate_id = 3 and (\"$key\" = '' or concat(c.company_name_en, c.company_name_vi, c.company_name_ko) like concat('%', \"$key\", '%') ) limit 10";
        $b2b = DB::select(DB::raw($b));

        $i = "select c.* from companies c  where cate_id = 2 and (\"$key\" = '' or concat(c.company_name_en, c.company_name_vi, c.company_name_ko) like concat('%', \"$key\", '%') ) limit 10";
        $incubating = DB::select(DB::raw($i));

        $e = "select p.* from posts p where cate_id = 2 and  (\"$key\" = '' or concat(p.title_en, p.content_en, p.title_ko,p.content_ko, p.title_vi,p.content_vi) like concat('%', \"$key\", '%') ) limit 10";
        $event = DB::select(DB::raw($e));

        $h ="select p.* from posts p where cate_id = 3 and  (\"$key\" = '' or concat(p.title_en, p.content_en, p.title_ko,p.content_ko, p.title_vi,p.content_vi) like concat('%', \"$key\", '%') ) limit 10";
        $history = DB::select(DB::raw($h));

        $s = "select s.* from showrooms s where (\"$key\" = '' or concat(s.title_en, s.content_en, s.title_ko,s.content_ko, s.title_vi,s.content_vi) like concat('%', \"$key\", '%') ) limit 10";
//        echo $s;die;
        $showrom = DB::select(DB::raw($s));

        return view('client.search.index', compact(['event','history','b2b','incubating' ,'locale','showrom','key']));
    }

    //function search b2b matching
    public  function  searchB2b(Request $request)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $key = $request->get('search');
        $b = "select c.* from companies c  where cate_id = 3 and (\"$key\" = '' or concat(c.company_name_en, c.company_name_vi, c.company_name_ko) like concat('%', \"$key\", '%') ) limit 10";
        $b2b = DB::select(DB::raw($b));
        return view('client.search.search-b2b',compact(['locale','b2b','key']));
    }

    //function search new
    public  function searchNew(Request $request)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $key = $request->get('search');
        $p ="select p.* from posts p where cate_id = 1 and   (\"$key\" = '' or concat(p.title_en, p.content_en, p.title_ko,p.content_ko, p.title_vi,p.content_vi) like concat('%', \"$key\", '%') ) limit 5";
        $new = DB::select(DB::raw($p));
        $h ="select p.* from posts p where cate_id = 3 and  (\"$key\" = '' or concat(p.title_en, p.content_en, p.title_ko,p.content_ko, p.title_vi,p.content_vi) like concat('%', \"$key\", '%') ) limit 10";
        $histories = DB::select(DB::raw($h));
        return view('client.search.search-new',compact(['new','histories','locale','key']));
    }

    public  function searchCompany( Request $request)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $key = $request->get('search');
        $b = "select c.* from companies c  where cate_id = 3 and (\"$key\" = '' or concat(c.company_name_en, c.company_name_vi, c.company_name_ko) like concat('%', \"$key\", '%') ) limit 5";
        $b2b_matching = DB::select(DB::raw($b));
        $i = "select c.* from companies c  where cate_id = 2 and (\"$key\" = '' or concat(c.company_name_en, c.company_name_vi, c.company_name_ko) like concat('%', \"$key\", '%') ) limit 5";
        $incubating = DB::select(DB::raw($i));
        return view('client.search.search-company',compact(['locale','key','incubating','b2b_matching']));
    }

    //function Search Company

    public  function searchEvent(Request $request)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $key = $request->get('search');
        $p ="select p.* from posts p where cate_id = 2 and   (\"$key\" = '' or concat(p.title_en, p.content_en, p.title_ko,p.content_ko, p.title_vi,p.content_vi) like concat('%', \"$key\", '%') ) limit 5";
        $event = DB::select(DB::raw($p));
        $h ="select p.* from posts p where cate_id = 3 and  (\"$key\" = '' or concat(p.title_en, p.content_en, p.title_ko,p.content_ko, p.title_vi,p.content_vi) like concat('%', \"$key\", '%') ) limit 10";
        $histories = DB::select(DB::raw($h));
        return view('client.search.search-event',compact(['locale','key','event','histories']));
    }


    ///search showroom///
    public function searchShowroom(Request $request)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $key = $request->get('search');

        $s =  $h ="select p.* from showrooms p where   (\"$key\" = '' or concat(p.title_en, p.content_en, p.title_ko,p.content_ko, p.title_vi,p.content_vi) like concat('%', \"$key\", '%') ) limit 10";
        $showroom = DB::select(DB::raw($s));
        return view('client.search.search-showroom',compact(['locale','key','showroom']));
    }
}
