<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App;
use Config;
use Illuminate\Support\Facades\DB;
use App\Model\Showroom;
use App\Model\Post;
use App\Model\Company;

class MobileController extends Controller
{
    public function index()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $b = "select * from companies where cate_id = 3 ORDER BY RAND() limit 5 ";
        $b2b_matching = DB::select(DB::raw($b));

        $i = "select * from companies where cate_id = 2 ORDER BY RAND() limit 5";
        $incubating = DB::select(DB::raw($i));

        $h = "select * from posts where cate_id = 3 ORDER BY RAND() limit 5";
        $history = DB::select(DB::raw($h));

        $e = "select * from posts where cate_id = 2 ORDER BY RAND() limit 5";
        $event = DB::select(DB::raw($e));

        $s = "select * from showrooms ORDER BY RAND() limit 5";
        $showrom = DB::select(DB::raw($s));
        return view('mobile.index', compact(['b2b_matching', 'incubating', 'showrom', 'locale', 'event', 'history']));

    }

    //function show all showroom
    public function showroomAll()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $showroom = Showroom::paginate(10);
        return view('mobile.showroom', compact(['showroom', 'locale']));
    }

    //showroom detail
    public function showroom($id)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }

        $showroom = Showroom::find($id);
        return view('mobile.showroom-detail', compact(['showroom', 'locale']));
    }

    public function showNew()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $new = DB::table('posts')->where('cate_id', 1)->paginate(5);

        $histories = Post::where('cate_id', 3)->paginate(10);
        return view('mobile.new', compact(['new', 'histories', 'locale']));
    }

    public function showEvent()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $event = DB::table('posts')->where('cate_id', 2)->paginate(5);

        $histories = Post::where('cate_id', 3)->paginate(10);
        return view('mobile.event', compact(['event', 'histories', 'locale']));
    }

    public function showBook()
    {
        return view('mobile.book');
    }

    public function showDetailCompany($id)
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $company = Company::find($id);
        return view('mobile.company-detail', compact(['locale', 'company']));
    }

    public function showCompany()
    {
        App::setLocale(Session::get('locale'));
        $locale = Config::get('app.locale');
        if ($locale == null) {
            $locale = 'en';
        }
        $company = Company::paginate(10);
        return view('mobile.company',compact(['locale','company']));

    }

}
