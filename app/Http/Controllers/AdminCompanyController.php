<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Repositories\Eloquent\CompanyEloquentRepository;
use  App\Repositories\Eloquent\PostEloquentRepository;
use  App\Repositories\Eloquent\ShowroomEloquentRepository;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use App;
use Config;
use Config\Setting;
use App\Model\Company;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Storage;
class AdminCompanyController extends Controller
{
    protected $companyRepository;

    function __construct(
        CompanyEloquentRepository $companyRepository
    )
    {
        // parent::__construct();
        $this->companyRepository = $companyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAdmin() {
        return view('backend.book.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.company.list');
    }

    public function B2b()
    {
        return view('backend.company.b2bmatching');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public  function showDataIncubating()
    {
        $sql = DB::table('companies')->select(['id', 'company_name_en', 'company_name_ko', 'company_name_vi','cate_id','address'])
            ->where('cate_id',2);
        $token = Session::token();
        return Datatables::of($sql)
            ->addColumn('action', function ($post) use ($token) {
                return '
                <a href="/admin/company/' . $post->id . '/edit" class="btn btn-xs btn-primary btn_datatable"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <a href="javascript:void(0);" onclick="return confirmDelete(' . $post->id . ')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                 <form action="/admin/company/' . $post->id . '" method="post" id="frm_delete_' . $post->id . '">
                    <input type="hidden" name="_token" id="csrf-token" value="' . $token . '" />
                    <input type="hidden" name="_method" value="DELETE">
                </form>';
            })
            ->make(true);
    }

    public  function showDataB2b()
    {
        $sql = DB::table('companies')->select(['id', 'company_name_en', 'company_name_ko', 'company_name_vi','cate_id','address'])
            ->where('cate_id',3);;
        $token = Session::token();
        return Datatables::of($sql)
            ->addColumn('action', function ($post) use ($token) {
                return '
                <a href="/admin/company/' . $post->id . '/edit" class="btn btn-xs btn-primary btn_datatable"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <a href="javascript:void(0);" onclick="return confirmDelete(' . $post->id . ')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</a>
                 <form action="/admin/company/' . $post->id . '" method="post" id="frm_delete_' . $post->id . '">
                    <input type="hidden" name="_token" id="csrf-token" value="' . $token . '" />
                    <input type="hidden" name="_method" value="DELETE">
                </form>';
            })
            ->make(true);
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->hasFile('img')) {
            $image = $request->file('img');
            $filename = str_random(4).$image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(268, 180);
            $image_resize->save(public_path('uploads/' . $filename));
            $urlFile = 'uploads/' . $filename;
        } else {
            $urlFile = "uploads/noimage.jpg";
        }
        //excel
        if ($request->hasFile('file_excel')) {
            $excel = $request->file('file_excel')->store('documents');
            $url_excel = $excel;
        } else{
            $url_excel = "";

        }


        //pdf
        if ($request->hasFile('file_pdf')) {
            $pdf =  $request->file('file_pdf')->store('documents');
            $url_pdf = $pdf;
        } else {
            $url_pdf = "";
        }

        $company = new Company;
        $company->company_name_en = $request->get('company_name_en');
        $company->company_name_ko = $request->get('company_name_ko');
        $company->company_name_vi = $request->get('company_name_vi');
        $company->images = $urlFile;
        $company->cate_id = $request->get('cate_id');
        $company->kind = $request->get('kind');
        $company->website = $request->get('website');
        $company->address = $request->get('address');
        $company->tel1 = $request->get('tel1');
        $company->otel = $request->get('otel');
        $company->email1 = $request->get('email1');
        $company->email2 = $request->get('email2');
        $company->about_us_en = $request->get('about_us_en');
        $company->about_us_ko = $request->get('about_us_ko');
        $company->about_us_vi = $request->get('about_us_vi');
        $company->mtel = $request->get('mtel');
        $company->manger = $request->get('manger');
        $company->area = $request->get('area');
        $company->type = $request->get('type');
        $company->date = $request->get('date');
        $company->emp_ko = $request->get('emp_ko');
        $company->emp_VN = $request->get('emp_VN');
        $company->location = $request->get('location');
        $company->owmer = $request->get('owmer');
        $company->file_excel = $url_excel;
        $company->file_pdf = $url_pdf;
        $company->service_en = $request->get('service_en');
        $company->service_ko = $request->get('service_ko');
        $company->service_vi = $request->get('service_vi');
        $company->owmer = $request->get('owmer');
        $company->save();

        return redirect()->back()->with('message', 'Create successful!');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $company = $this->companyRepository->find($id);
       return view('backend.company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        if ($request->hasFile('img')) {
            $image = $request->file('img');
            $filename = str_random(4).$image->getClientOriginalName();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(268, 180);
            $image_resize->save(public_path('uploads/' . $filename));
            $urlFile = 'uploads/' . $filename;
        } else {
            $urlFile = $company->images;
        }

        //excel
        if ($request->hasFile('file_excel')) {
            $excel = $request->file('file_excel')->store('documents');
            $url_excel = $excel;
        } else{
            $url_excel = $company->file_excel;
        }
        //pdf
        if ($request->hasFile('file_pdf')) {
            $pdf =  $request->file('file_pdf')->store('documents');
            $url_pdf = $pdf;
        } else {
            $url_pdf = $company->file_pdf;
        }

        $company->company_name_en = $request->get('company_name_en');
        $company->company_name_ko = $request->get('company_name_ko');
        $company->company_name_vi = $request->get('company_name_vi');
        $company->images = $urlFile;
        $company->cate_id = $request->get('cate_id');
        $company->kind = $request->get('kind');
        $company->website = $request->get('website');
        $company->address = $request->get('address');
        $company->tel1 = $request->get('tel1');
        $company->otel = $request->get('otel');
        $company->email1 = $request->get('email1');
        $company->email2 = $request->get('email2');
        $company->about_us_en = $request->get('about_us_en');
        $company->about_us_ko = $request->get('about_us_ko');
        $company->about_us_vi = $request->get('about_us_vi');
        $company->mtel = $request->get('mtel');
        $company->manger = $request->get('manger');
        $company->area = $request->get('area');
        $company->type = $request->get('type');
        $company->date = $request->get('date');
        $company->emp_ko = $request->get('emp_ko');
        $company->emp_VN = $request->get('emp_VN');
        $company->location = $request->get('location');
        $company->owmer = $request->get('owmer');
        $company->file_excel = $url_excel;
        $company->file_pdf = $url_pdf;
        $company->service_en = $request->get('service_en');
        $company->service_ko = $request->get('service_ko');
        $company->service_vi = $request->get('service_vi');

        $company->save();
        return redirect()->back()->with('message', 'Update successful!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = $this->companyRepository->delete($id);
        return redirect()->back()->with('message', 'Delete successful!');
    }
}
