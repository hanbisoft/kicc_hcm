function SubtractDays(toAdd) {
    if (!toAdd || toAdd == '' || isNaN(toAdd)) return;
    var d = new Date();
    d.setDate(d.getDate() - parseInt(toAdd));

    document.getElementById("result").innerHTML = d.getDate() + "/" + d.getMonth() + "/" + d.getFullYear();
}

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

var dataResult = [];
var dataUpdate = [];
this.callApi();

//function get api
function callApi() {
    $.ajax({
        url: "/api/book-pdf",
      
        beforeSend: function () {
            $('#wait').show();
        },
        complete: function () {
            $('#wait').hide();
        },
        data: {
            'room_id':1
        },
        type: "get",
        dateType: "json",
        success: function (result) {
            console.log(result)
            dataUpdate = result.map(x=>x);
            //update list
            setCalendar(dataUpdate)
        }
    });
}
function setCalendar(dataUpdate)
{
    console.log(dataUpdate)
    var dp = new DayPilot.Calendar("dp", {
        viewType: "WorkWeek",
        headerDateFormat: "yyyy/mM/dd",
        days: 20,
        columnWidthSpec: "Auto",
        columnHeightSpec: "Auto",
        cellDuration: 30,
        businessBeginsHour: 0,
        businessEndsHour: 13,
        dayBeginsHour: 9,
        dayEndsHour: 19,
        cellHeight: 50,
        columnWidth: 100,
        eventArrangement: "Full",
        timeRangeSelectedHandling: "Disabled",
        eventDeleteHandling: "Disabled",
        eventMoveHandling: "Disabled",
        eventResizeHandling: "Disabled",
        eventClickHandling: "Disabled",
        eventHoverHandling: "Disabled",
    });
    dp.events.list = dataUpdate;

    dp.init();
    console.log(DayPilot.Date.today().addHours(11))
    var pdfConfig = {
        "letter-portrait": {
            orientation: "portrait",
            unit: "in",
            format: "letter",
            maxWidth: 7,
            maxHeight: 9,
            left: 0.5,
            top: 1,
            space: 0.2
        },
        "letter-landscape": {
            orientation: "landscape",
            unit: "in",
            format: "letter",
            maxWidth: 9,
            maxHeight: 7,
            left: 0.5,
            top: 1,
            space: 0.2
        },
        "a4-portrait": {
            orientation: "portrait",
            unit: "cm",
            format: "a4",
            maxWidth: 18,
            maxHeight: 23,
            left: 1.5,
            top: 3,
            space: 0.5
        },
        "a4-landscape": {
            orientation: "landscape",
            unit: "cm",
            format: "a4",
            maxWidth: 23,
            maxHeight: 18,
            left: 1.5,
            top: 3,
            space: 0.5
        }
    };

    (function () {
        document.getElementById("export").addEventListener("click", function (e) {
            var orientation = document.getElementById("pdf-orientation").value;
            var format = document.getElementById("pdf-format").value;
            var config = pdfConfig[format + "-" + orientation];
            var blob = createPdfAsBlobOnePage(config);
            DayPilot.Util.downloadBlob(blob, "calendar.pdf");
        });
    })();

    function createPdfAsBlobOnePage(config) {
        var pdf = new jsPDF(config.orientation, config.unit, config.format);
        pdf.setFontSize(20);
        pdf.text(config.left, config.top, "Reservation");

        var image = dp.exportAs("jpeg", {
            scale: 2,
            quality: 0.95
        });

        var dimensions = image.dimensions();  // pixels
        var maxDimensions = {width: config.maxWidth, height: config.maxHeight};   // inches
        var adjusted = shrink(dimensions, maxDimensions);

        pdf.addImage(image.toDataUri(), 'JPEG', config.left, config.top + config.space, adjusted.width, adjusted.height);

        return pdf.output("blob");
    }

    function shrink(dimensions, max) {
        var widthRatio = dimensions.width / max.width;
        var heightRatio = dimensions.height / max.height;

        var ratio = Math.max(widthRatio, heightRatio);
        ratio = Math.max(ratio, 1);

        var width = dimensions.width / ratio;
        var height = dimensions.height / ratio;
        return {width: width, height: height};
    }
}