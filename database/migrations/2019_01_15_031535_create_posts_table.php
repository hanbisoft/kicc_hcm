<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en')->nullable();
            $table->string('title_vi')->nullable();
            $table->string('title_ko')->nullable();
            $table->string('slug')->nullable();
            $table->string('description')->nullable();
            $table->integer('day')->nullable();
            $table->string('month')->nullable();
            $table->time('time_start')->nullable();
            $table->time('time_end')->nullable();
            $table->text('content_en')->nullable();
            $table->text('content_vi')->nullable();
            $table->text('content_ko')->nullable();
            $table->string('images')->nullable();;
            $table->integer('cate_id')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
