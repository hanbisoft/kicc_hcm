<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(App\Model\Company::class, function (Faker $faker) {
    $title = $faker->text($maxNbChars = 60);
    return [
        'title_en' => $title,
        'content_en' => $faker->text($maxNbChars = 1500),
        'title_vi' => $title,
        'content_vi' => $faker->text($maxNbChars = 1500),
        'title_ko' => $title,
        'content_ko' => $faker->text($maxNbChars = 1500),
        'images'=>'uploads/matching2.jpg',
        'cate_id'=> 3,
        'created_at' => new DateTime,
        'updated_at' => new DateTime,
    ];
});
