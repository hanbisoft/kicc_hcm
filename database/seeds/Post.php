<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class Post extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title_en' => str_random(150),
            'description_en' =>  str_random(150),
            'content_en' =>str_random(500),
            'title_vi' => str_random(150),
            'description_vi' =>  str_random(150),
            'content_vi' =>str_random(500),
            'title_ko' => str_random(150),
            'description_ko' =>  str_random(150),
            'content_ko' =>str_random(500),
            'images'=>'uploads/product_03.jpg',
            'cate_id'=> 1,
        ]);
    }
}
